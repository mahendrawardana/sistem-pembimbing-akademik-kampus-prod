<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::group(['namespace' => 'Mahasiswa', 'middleware' => 'authLogin'], function () {

    /**
     * Mahasiswa Management
     */
    Route::get('/mahasiswa', "Mahasiswa@admin_mahasiswa_list")->name('admin_mahasiswa_list');
    Route::get('/mahasiswa/create', "Mahasiswa@admin_mahasiswa_create")->name('admin_mahasiswa_create');
    Route::post('/mahasiswa/insert', "Mahasiswa@admin_mahasiswa_insert")->name('admin_mahasiswa_insert');
    Route::get('/mahasiswa/edit/{id_mahasiswa}', "Mahasiswa@admin_mahasiswa_edit")->name('admin_mahasiswa_edit');
    Route::post('/mahasiswa/update/{id_mahasiswa}', "Mahasiswa@admin_mahasiswa_update")->name('admin_mahasiswa_update');
    Route::delete('/mahasiswa/delete/{id_mahasiswa}', "Mahasiswa@admin_mahasiswa_delete")->name('admin_mahasiswa_delete');

    /**
     * Dosen Mahasiswa Nilai Management
     */
    Route::get('/dosen/mahasiswa', "Mahasiswa@dosen_mahasiswa_list")->name('dosen_mahasiswa_list');
    Route::get('/dosen/mahasiswa/nilai', "Mahasiswa@dosen_mahasiswa_nilai")->name('dosen_mahasiswa_nilai');
});

Route::group(['namespace' => 'Dosen', 'middleware' => 'authLogin'], function () {

    /**
     * Dosen Management
     */
    Route::get('/dosen', "Dosen@index")->name('dosen_list');
    Route::get('/dosen/create', "Dosen@create")->name('dosen_create');
    Route::post('/dosen/insert', "Dosen@insert")->name('dosen_insert');
    Route::get('/dosen/edit/{id_dosen}', "Dosen@edit")->name('dosen_edit');
    Route::post('/dosen/update/{id_dosen}', "Dosen@update")->name('dosen_update');
    Route::delete('/dosen/delete/{id_dosen}', "Dosen@delete")->name('dosen_delete');
});

Route::group(['namespace' => 'MataKuliah', 'middleware' => 'authLogin'], function () {

    /**
     * Mata Kuliah Management
     */
    Route::get('/mata-kuliah', "MataKuliah@index")->name('mata_kuliah_list');
    Route::get('/mata-kuliah/create', "MataKuliah@create")->name('mata_kuliah_create');
    Route::post('/mata-kuliah/insert', "MataKuliah@insert")->name('mata_kuliah_insert');
    Route::get('/mata-kuliah/edit/{id_mata_kuliah}', "MataKuliah@edit")->name('mata_kuliah_edit');
    Route::post('/mata-kuliah/update/{id_mata_kuliah}', "MataKuliah@update")->name('mata_kuliah_update');
    Route::delete('/mata-kuliah/delete/{id_mata_kuliah}', "MataKuliah@delete")->name('mata_kuliah_delete');
});

Route::group(['namespace' => 'Krs', 'middleware' => 'authLogin'], function () {

    /**
     * Admin KRS Management
     */
    Route::get('/admin/krs', "Krs@admin_list")->name('admin_krs_list');
    Route::get('/admin/krs/create', "Krs@admin_create")->name('admin_krs_create');
    Route::post('/admin/krs/insert', "Krs@admin_insert")->name('admin_krs_insert');
    Route::get('/admin/krs/edit/{id_krs}', "Krs@admin_edit")->name('admin_krs_edit');
    Route::post('/admin/krs/update/{id_krs}', "Krs@admin_update")->name('admin_krs_update');
    Route::delete('/admin/krs/delete/{id_krs}', "Krs@admin_delete")->name('admin_krs_delete');
    Route::get('/admin/krs/detail/{id_mahasiswa}', "Krs@admin_detail")->name('admin_krs_detail');

    /**
     * Dosen KRS Management
     */
    Route::get('/dosen/krs', "Krs@dosen_list")->name('dosen_krs_list');
    Route::get('/dosen/krs/detail/{id_mahasiswa}', "Krs@dosen_detail")->name('dosen_krs_detail');
    Route::get('/dosen/krs/setuju/{id_mahasiswa}', "Krs@dosen_setuju")->name('dosen_krs_setuju');
    Route::get('/dosen/krs/tolak/{id_mahasiswa}', "Krs@dosen_tolak")->name('dosen_krs_tolak');

    /**
     * Mahasiswa KRS Management
     */
    Route::get('/mahasiswa/krs/list', "Krs@mahasiswa_list")->name('mahasiswa_krs_list');
    Route::get('/mahasiswa/krs/create', "Krs@mahasiswa_create")->name('mahasiswa_krs_create');
    Route::post('/mahasiswa/krs/insert', "Krs@mahasiswa_insert")->name('mahasiswa_krs_insert');
    Route::get('/mahasiswa/krs/edit/{id_krs}', "Krs@mahasiswa_edit")->name('mahasiswa_krs_edit');
    Route::post('/mahasiswa/krs/update/{id_krs}', "Krs@mahasiswa_update")->name('mahasiswa_krs_update');
    Route::delete('/mahasiswa/krs/delete/{id_krs}', "Krs@mahasiswa_delete")->name('mahasiswa_krs_delete');
    Route::get('/mahasiswa/krs/ajukan', "Krs@mahasiswa_ajukan")->name('mahasiswa_krs_ajukan');
});

Route::group(['namespace' => 'jadwalPerwalian', 'middleware' => 'authLogin'], function () {


    /**
     * Admin Jadwal Perwalian Management
     */
    Route::get('/admin/jadwal-perwalian', "JadwalPerwalian@admin_list")->name('adminJadwalPerwalianList');
    Route::get('/admin/jadwal-perwalian/create', "JadwalPerwalian@admin_create")->name('adminJadwalPerwalianCreate');
    Route::post('/admin/jadwal-perwalian/insert', "JadwalPerwalian@admin_insert")->name('adminJadwalPerwalianInsert');
    Route::get('/admin/jadwal-perwalian/edit/{id_jadwal_perwalian}', "JadwalPerwalian@admin_edit")->name('adminJadwalPerwalianEdit');
    Route::post('/admin/jadwal-perwalian/update/{id_jadwal_perwalian}', "JadwalPerwalian@admin_update")->name('adminJadwalPerwalianUpdate');
    Route::delete('/admin/jadwal-perwalian/delete/{id_jadwal_perwalian}', "JadwalPerwalian@admin_delete")->name('adminJadwalPerwalianDelete');

    /**
     * Dosen Jadwal Perwalian Management
     */
    Route::get('/dosen/jadwal-perwalian', "JadwalPerwalian@dosen_list")->name('dosenJadwalPerwalianList');
    Route::get('/dosen/jadwal-perwalian/create', "JadwalPerwalian@dosen_create")->name('dosenJadwalPerwalianCreate');
    Route::post('/dosen/jadwal-perwalian/insert', "JadwalPerwalian@dosen_insert")->name('dosenJadwalPerwalianInsert');


    Route::get('/dosen/jadwal-perwalian/edit/{id_jadwal_perwalian}', "JadwalPerwalian@dosen_edit")->name('dosenJadwalPerwalianEdit');
    Route::post('/dosen/jadwal-perwalian/update/{id_jadwal_perwalian}', "JadwalPerwalian@dosen_update")->name('dosenJadwalPerwalianUpdate');
    Route::get('/dosen/jadwal-perwalian/presensi/{id_jadwal_perwalian}', "JadwalPerwalian@dosen_presensi")->name('dosenJadwalPerwalianPresensi');
    Route::post('/dosen/jadwal-perwalian/presensi-update/{id_jadwal_perwalian}', "JadwalPerwalian@dosen_presensi_update")->name('dosenJadwalPerwalianPresensiUpdate');
    Route::get('/dosen/jadwal-perwalian/catatan/{id_jadwal_perwalian}', "JadwalPerwalian@dosen_catatan")->name('dosenJadwalPerwalianCatatan');
    Route::post('/dosen/jadwal-perwalian/catatan/update/{id_jadwal_perwalian}', "JadwalPerwalian@dosen_catatan_update")->name('dosenJadwalPerwalianCatatanUpdate');

    /**
     * Mahasiswa Jadwal Perwalian Management
     */
    Route::get('/mahasiswa/jadwal-perwalian', "JadwalPerwalian@mahasiswa_list")->name('mahasiswaJadwalPerwalianList');
    Route::get('/mahasiswa/jadwal-perwalian/catatan/{id_jadwal_perwalian}', "JadwalPerwalian@mahasiswa_catatan")->name('mahasiswaJadwalPerwalianCatatan');
});

Route::group(['namespace' => 'TranskipNilai', 'middleware' => 'authLogin'], function () {

    /**
     * Admin Transkip Nilai Management
     */
    Route::get('/transkip-nilai/mahasiswa', "TranskipNilai@mahasiswa_list")->name('transkip_nilai_mahasiswa_list');
    Route::get('/transkip-nilai/nilai/list/{id_mahasiswa}', "TranskipNilai@nilai_list")->name('transkip_nilai_mahasiswa_nilai_list');
    Route::get('/transkip-nilai/nilai/edit/{id_krs}', "TranskipNilai@nilai_edit")->name('transkip_nilai_mahasiswa_nilai_edit');
    Route::post('/transkip-nilai/nilai/update/{id_krs}', "TranskipNilai@nilai_update")->name('transkip_nilai_mahasiswa_nilai_update');
    Route::delete('/transkip-nilai/nilai/delete/{id_krs}', "TranskipNilai@nilai_delete")->name('transkip_nilai_mahasiswa_nilai_delete');

    Route::get('/transkip-nilai/nilai/create/{id_mahasiswa}', "TranskipNilai@nilai_create")->name('transkip_nilai_mahasiswa_nilai_create');
    Route::post('/transkip-nilai/nilai/insert/{id_mahasiswa}', "TranskipNilai@nilai_insert")->name('transkip_nilai_mahasiswa_nilai_insert');

    /**
     * Mahasiswa Transkip Nilai Management
     */
    Route::get('/transkip-nilai/nilai/mahasiswa', "TranskipNilai@mahasiswa_nilai_detail")->name('transkip_nilai_mahasiswa_detail');
});

Route::group(['namespace' => 'InformasiUmum', 'middleware' => 'authLogin'], function () {

    /**
     * Informasi Umum Management
     */
    Route::get('/informasi-umum', "InformasiUmum@list")->name('informasi_umum_list');
    Route::get('/informasi-umum/create', "InformasiUmum@create")->name('informasi_umum_create');
    Route::get('/informasi-umum/edit', "InformasiUmum@edit")->name('informasi_umum_edit');
});

Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
    Route::post('city/list', "General@city_list")->name('cityList');
    Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');
    });

});

/*Route::namespace('LandingPage')->group(function () {

    Route::get('/', 'LandingPage@home')->name('landing_page_home');
    Route::get('/informasi-umum', 'LandingPage@informasi_umum_list')->name('landing_page_informasi_umum_list');
    Route::get('/informasi-umum/detail', 'LandingPage@informasi_umum_detail')->name('landing_page_informasi_umum_detail');
    Route::get('/kontak-kami', 'LandingPage@kontak_kami')->name('landing_page_kontak_kami');

});*/

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

