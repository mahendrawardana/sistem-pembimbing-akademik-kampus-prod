<?php
/**
 * Created by PhpStorm.
 * User: mahendrawardana
 * Date: 07/02/19
 * Time: 13.36
 */

return [
    'acc_api_token' => 'c890cc0bd6562ed5b3a2a1105d6ddb9af9fdadab0c55216255bb0465d23c604aab845955b7b43787392c108f5a8cc82292435c99e7e93720145d89193cac06ba',
    'prefixProduksi' => 'MKK-',
    'ppnPersen' => 10,
    'decimalStep' => .01,
    'kodeHutangSupplier' => 'HS',
    'kodePreOrder' => 'PO',
    'kodeHutangLain' => 'HL',
    'kodePiutangPelanggan' => 'PP',
    'kodePiutangLain' => 'PL',
    'kodePenyesuaianStokBahan' => 'PSB',
    'kodePenyesuaianStokProduk' => 'PSP',
    'kodeJurnalUmum' => 'JU',
    'kodeAsset' => 'AT',
    'kodeKategoryAsset' => 'KAS',
    'bankType' => 'BCA',
    'bankRekening' => '4161877888',
    'bankAtasNama' => 'BAMBANG PRANOTO',
    'companyName' => 'Sistem Klinik',
    'companyAddress' => 'JL. Jendral A Yani, No. 04, Baler Bale Agung, Negara',
    'companyPhone' => '0818 0260 9624',
    'companyTelp' => '(0365) 41100',
    'companyEmail' => 'kutuskutusbali@gmail.com',
    'companyBendahara' => 'ARNIEL',
    'companyTuju' => 'Bapak Angga',
    'topMenu' => [
        'dashboard' => 'dashboard',

        'mahasiswa' => 'mahasiswa',
        'dosen' => 'dosen',
        'mata_kuliah' => 'mata_kuliah',
        'krs' => 'krs',
        'jadwal_perwalian_admin' => 'jadwal_perwalian_admin',
        'profil_mahasiswa' => 'profil_mahasiswa',
        'transkip_nilai' => 'transkip_nilai',
        'informasi_umum' => 'informasi_umum',
        'daftar_mahasiswa' => 'daftar_mahasiswa',
        'daftar_krs_mahasiswa' => 'daftar_krs_mahasiswa',
        'jadwal_perwalian_dosen' => 'jadwal_perwalian_dosen',
        'jadwal_perwalian_mahasiswa' => 'jadwal_perwalian_mahasiswa',
        'lihat_nilai' => 'lihat_nilai',
        'krs_mahasiswa' => 'krs_mahasiswa',

        'masterData' => 'master_data',
        'master_satuan' => 'satuan',
        'master_jenis_barang' => 'jenis_barang',
        'master_supplier' => 'supplier',
        'master_barang' => 'barang',
        'master_pelanggan' => 'pelanggan',
        'master_role_user' => 'role_user',
        'master_user' => 'user',
        'master_staf' => 'staf',
    ]
];
