<form action="{{ route('supplierUpdate', ['id'=>Main::encrypt($edit->id_supplier)]) }}" method="post" class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Supplier</label>
                            <input type="text" class="form-control m-input" name="spl_kode" value="{{ $edit->spl_kode }}" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Supplier</label>
                            <input type="text" class="form-control m-input" name="spl_nama" value="{{ $edit->spl_nama }}" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Telepon/WhatsApp</label>
                            <input type="text" class="form-control m-input" name="spl_phone" value="{{ $edit->spl_phone }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Email</label>
                            <input type="text" class="form-control m-input" name="spl_email" value="{{ $edit->spl_email }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat</label>
                            <textarea class="form-control" name="spl_alamat">{{ $edit->spl_alamat }}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Keterangan</label>
                            <textarea class="form-control" name="spl_keterangan">{{ $edit->spl_keterangan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>