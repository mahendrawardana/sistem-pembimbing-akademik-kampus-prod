<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Mastery a Education Category Bootstrap Responsive Website Template | Home :: W3layouts</title>
    <!-- web fonts -->
    <link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&display=swap"
          rel="stylesheet">
    <!-- //web fonts -->
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('template/landing_page/css/style-starter.css') }}">
</head>

<body>


<!-- Top Menu 1 -->
<section class="w3l-top-menu-1">
    <div class="top-hd">
        <div class="container">
            <header class="row top-menu-top">
                <div class="accounts col-md-9 col-6">
                    <li class="top_li"><span class="fa fa-phone"></span><a href="tel:+142 5897555">+142 5897555</a></li>
                    <li class="top_li1"><span class="fa fa-envelope-o"></span> <a
                                href="mailto:education-mail@support.com" class="mail"> info@example.com</a></li>
                </div>
                <div class="social-top col-md-3 col-6">
                    <a href="{{ route('loginPage') }}" class="btn btn-secondary btn-theme4">Login</a>
                </div>
            </header>
        </div>
    </div>
</section>
<!-- //Top Menu 1 -->
<section class="w3l-bootstrap-header">
    <nav class="navbar navbar-expand-lg navbar-light py-lg-2 py-2">
        <div class="container">
            <a class="navbar-brand" href="index.html"><span class="fa fa-pencil-square-o "></span> Mastery</a>
            <!-- if logo is image enable this
<a class="navbar-brand" href="#index.html">
    <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
</a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon fa fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('landing_page_home') }}">Beranda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('landing_page_informasi_umum_list') }}">Informasi Umum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('landing_page_kontak_kami')  }}">Kontak Kami</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>

<!-- Content Utama -->
@yield('content')
<!-- Content Utama -->

<!-- grids block 5 -->
<section class="w3l-footer-29-main">
    <div class="footer-29">
        <div class="container">
            <div class="row">
                <div class="col-md-6 footer-top-29">
                    <div class="footer-list-29 footer-1">
                        <h6 class="footer-title-29">Contact Us</h6>
                        <ul>
                            <li>
                                <p><span class="fa fa-map-marker"></span> Estate Business, #32841 block, #221DRS Real
                                    estate business building, UK.</p>
                            </li>
                            <li><a href="tel:+7-800-999-800"><span class="fa fa-phone"></span> +(21)-255-999-8888</a>
                            </li>
                            <li><a href="mailto:corporate-mail@support.com" class="mail"><span
                                            class="fa fa-envelope-open-o"></span> corporate-mail@support.com</a></li>
                        </ul>
                        <div class="main-social-footer-29">
                            <a href="#facebook" class="facebook"><span class="fa fa-facebook"></span></a>
                            <a href="#twitter" class="twitter"><span class="fa fa-twitter"></span></a>
                            <a href="#google-plus" class="google-plus"><span class="fa fa-google-plus"></span></a>
                            <a href="#linkedin" class="linkedin"><span class="fa fa-linkedin"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ml-auto footer-list-29 footer-4">
                    <ul>
                        <h6 class="footer-title-29">Quick Links</h6>
                        <li><a href="{{ route('landing_page_home') }}">Beranda</a></li>
                        <li><a href="{{ route('landing_page_informasi_umum_list') }}">Informasi Umum</a></li>
                        <li><a href="{{ route('landing_page_kontak_kami')  }}">Kontak Kami</a></li>
                    </ul>
                </div>
            </div>
            <div class="d-grid grid-col-2 bottom-copies">
                <p class="copy-footer-29">© 2020 Coffee. All rights reserved | Designed by <a
                            href="https://w3layouts.com">W3layouts</a></p>
                <ul class="list-btm-29">
                    <li><a href="https://intiru.com/" target="_blank">Development by Intiru Digital</a></li>

                </ul>
            </div>
        </div>
    </div>
    <!-- move top -->
    <button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fa fa-angle-up"></span>
    </button>
</section>


<!-- //script -->
<script src="{{ asset('template/landing_page/js/jquery-3.3.1.min.js') }}"></script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('template/landing_page/js/jquery-3.4.1.slim.min.js') }}"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="{{ asset('template/landing_page/js/popper.min.js') }}"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="{{ asset('template/landing_page/js/bootstrap.min.js') }}"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>

<!-- Template JavaScript -->
<script src="{{ asset('template/landing_page/js/all.js') }}"></script>

<!-- <script src="assets/js/smoothscroll.js"></script> -->
<script src="{{ asset('template/landing_page/js/owl.carousel.js') }}"></script>

</body>

</html>
<!-- // grids block 5 -->