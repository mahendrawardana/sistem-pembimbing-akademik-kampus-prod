@extends('landing_page.index_landing_page')

@section('content')

    <!-- section banner -->
    <section class="w3l-about-breadcrum">
        <div class="breadcrum-bg py-sm-5 py-4">
            <div class="container py-lg-3">
                <h2>Detail Informasi Umum</h2>
                <p><a href="{{ route('landing_page_home') }}">Home</a> &nbsp; / &nbsp; <a
                            href="{{ route('landing_page_informasi_umum_list') }}"> Informasi Umum </a> &nbsp; / &nbsp;
                    Detail Informasi Umum</p>
            </div>
        </div>
    </section>
    <!-- section banner -->

    <!-- section detail informasi umum -->
    <section class="w3l-content-with-photo-4">
        <div id="content-with-photo4-block" class="pt-5">
            <div class="container py-md-5">
                <div class="cwp4-two row">

                    <div class="cwp4-text col-lg-6">
                        <h3>About Our Institution</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
                        </p>

                        <ul class="cont-4">
                            <li><span class="fa fa-check"></span>Materiality & Interpretation</li>
                            <li><span class="fa fa-check"></span>Design Management and Cultural Enterprise</li>
                            <li><span class="fa fa-check"></span>Experience Design (XD)</li>
                            <li><span class="fa fa-check"></span>Sound Design; Social Media and SEO</li>
                        </ul>
                    </div>
                    <div class="cwp4-image col-lg-6 pl-lg-5 mt-lg-0 mt-5">
                        <img src="{{ asset('template/landing_page/images/g4.jpg')  }}" class="img-fluid" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="w3l-features-1">
        <!-- /features -->
        <div class="features py-4">
            <div class="container pb-5">

                <div class="fea-gd-vv row ">
                    <div class="float-lt feature-gd col-lg-4 col-sm-6">

                        <div class="icon-info">
                            <h5>Our History 1998</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
                            </p>
                        </div>

                    </div>
                    <div class="float-mid feature-gd col-lg-4 col-sm-6 mt-sm-0 mt-4">

                        <div class="icon-info">
                            <h5>Over 100 Facalties</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
                            </p>
                        </div>
                    </div>
                    <div class="float-rt feature-gd col-lg-4 col-sm-6 mt-lg-0 mt-4">

                        <div class="icon-info">
                            <h5>We Have 12,000 Students</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- //features -->
    </section>
    <!-- section detail infromasi umum -->

@endsection