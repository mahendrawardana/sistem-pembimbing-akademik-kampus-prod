@extends('landing_page.index_landing_page')

@section('content')

    <!-- section banner -->
<section class="w3l-service-breadcrum">
    <div class="breadcrum-bg py-sm-5 py-4">
        <div class="container py-lg-3">
            <h2>Informasi Umum</h2>
            <p><a href="{{ route('landing_page_home') }}">Beranda</a> &nbsp; / &nbsp; Informasi Umum</p>
        </div>
    </div>
</section>
<!-- section banner -->

<!-- list informasi umum -->
<section class="w3l-features-8">
    <!-- /features -->
    <div class="features py-5" id="services">
        <div class="container py-md-3">


            <div class="fea-gd-vv text-center row">

                <div class="float-top col-lg-4 col-md-6">
                    <a href="{{ route('landing_page_informasi_umum_detail') }}">
                        <img src="{{ asset('template/landing_page/images/g2.jpg') }}" class="img-responsive" alt="">
                    </a>
                    <div class="float-lt feature-gd">
                        <h3><a href="{{ asset('template/landing_page/') }}">Robots in Space</a> </h3>
                        <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                    </div>
                </div>
                <div class="float-top col-lg-4 col-md-6 mt-md-0 mt-5">
                    <a href="#"><img src="{{ asset('template/landing_page/images/g3.jpg') }}" class="img-responsive" alt=""></a>
                    <div class="float-lt feature-gd">
                        <h3><a href="#">Differentiated Instruction</a> </h3>
                        <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                    </div>
                </div>
                <div class="float-top col-lg-4 col-md-6 mt-lg-0 mt-5">
                    <a href="#"><img src="{{ asset('template/landing_page/images/g8.jpg') }}" class="img-responsive" alt=""></a>
                    <div class="float-lt feature-gd">
                        <h3><a href="#">Analysis of Principal</a> </h3>
                        <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                    </div>
                </div>
                <div class="float-top col-lg-4 col-md-6 mt-5">
                    <a href="#"><img src="{{ asset('template/landing_page/images/g5.jpg') }}" class="img-responsive" alt=""></a>
                    <div class="float-lt feature-gd">
                        <h3><a href="#">The Sound of Silence</a> </h3>
                        <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                    </div>
                </div>
                <div class="float-top col-lg-4 col-md-6 mt-5">
                    <a href="#"><img src="{{ asset('template/landing_page/images/g6.jpg') }}" class="img-responsive" alt=""></a>
                    <div class="float-lt feature-gd">
                        <h3><a href="#">Formation Flying</a> </h3>
                        <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                    </div>
                </div>
                <div class="float-top col-lg-4 col-md-6 mt-5">
                    <a href="#"><img src="{{ asset('template/landing_page/images/g7.jpg') }}" class="img-responsive" alt=""></a>
                    <div class="float-lt feature-gd">
                        <h3><a href="#">Finger Gesture</a> </h3>
                        <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //features -->
</section>
<!-- list informasi umum -->

@endsection