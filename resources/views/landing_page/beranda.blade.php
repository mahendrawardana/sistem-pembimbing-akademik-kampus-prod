@extends('landing_page.index_landing_page')

@section('content')
    <!-- section slide -->
    <section class="w3l-main-slider" id="home">
        <!-- main-slider -->
        <div class="companies20-content">

            <div class="owl-one owl-carousel owl-theme">
                <div class="item">
                    <li>
                        <div class="slider-info banner-view bg bg2" data-selector=".bg.bg2">
                            <div class="banner-info">
                                <div class="container">
                                    <div class="banner-info-bg mx-auto text-center">
                                        <h5>Better Education For A Better World</h5>
                                        <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="services.html">Read
                                            More</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>
                </div>
                <div class="item">
                    <li>
                        <div class="slider-info  banner-view banner-top1 bg bg2" data-selector=".bg.bg2">
                            <div class="banner-info">
                                <div class="container">
                                    <div class="banner-info-bg mx-auto text-center">
                                        <h5>Explore The World Of Our Graduates</h5>
                                        <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="services.html">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </div>
                <div class="item">
                    <li>
                        <div class="slider-info banner-view banner-top2 bg bg2" data-selector=".bg.bg2">
                            <div class="banner-info">
                                <div class="container">
                                    <div class="banner-info-bg mx-auto text-center">
                                        <h5>Exceptional People, Exceptional Care</h5>
                                        <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="services.html">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </div>
                <div class="item">
                    <li>
                        <div class="slider-info banner-view banner-top3 bg bg2" data-selector=".bg.bg2">
                            <div class="banner-info">
                                <div class="container">
                                    <div class="banner-info-bg mx-auto text-center">
                                        <h5>Explore The World Of Our Graduates</h5>
                                        <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="services.html">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </div>
            </div>
        </div>
        <!-- /main-slider -->
    </section>
    <!-- section slide -->

    <!-- section jadwal perwalian list -->
    <section class="w3l-pricing-7" id="pricing">
        <div class="w3l-pricing-7_sur py-5">
            <div class="container py-md-3">
                <div class="heading text-center mx-auto">
                    <h3 class="head">See Our Packages</h3>
                    <p class="my-3 head"> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                        cubilia Curae;
                        Nulla mollis dapibus nunc, ut rhoncus
                        turpis sodales quis. Integer sit amet mattis quam.</p>
                </div>
                <div class="row w3l-pricing-7-gd-top pt-3 mt-5">
                    <div class="col-lg-4 col-md-6 w3l-pricing-7-gd-left">
                        <div class="w3l-pricing-7  pric-7-1">
                            <h6>Basic Pack</h6>
                            <div class="w3l-pricing-7-top">
                                <h3 class="title-sub">$199.00</h3>
                                <p>1 Month</p>
                            </div>
                            <div class="w3l-pricing-7-bottom">
                                <div class="w3l-pricing-7-bottom-bottom">
                                    <ul class="links">
                                        <li class="tick-info">
                                            Vestibulum ante ipsum
                                        </li>
                                        <li class="tick-info">
                                            Primis in faucibus
                                        </li>
                                        <li class="tick-info">
                                            Orci luctus et ultrices
                                        </li>
                                        <li class="tick-info">
                                            Posuere cubilia Curae
                                        </li>
                                        <li class="tick-info">
                                            Nulla mollis dapibus
                                        </li>
                                        <li class="tick-info">
                                            Rhoncus turpis sodales
                                        </li>
                                    </ul>
                                </div>
                                <div class="buy-button text-center mt-5">
                                    <a href="contact.html" class="btn btn-secondary btn-theme2">
                                        <div class="anim"></div>
                                        <span>Request Now</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-md-0 mt-4 w3l-pricing-7-gd-left active">
                        <div class="w3l-pricing-7 pric-7 active">
                            <h6>Standard Pack</h6>
                            <div class="w3l-pricing-7-top">
                                <h3 class="title-sub">$299.00</h3>
                                <p>1 Month</p>
                            </div>
                            <div class="w3l-pricing-7-bottom">
                                <div class="w3l-pricing-7-bottom-bottom">
                                    <ul class="links">
                                        <li class="tick-info">
                                            Vestibulum ante ipsum
                                        </li>
                                        <li class="tick-info">
                                            Primis in faucibus
                                        </li>
                                        <li class="tick-info">
                                            Orci luctus et ultrices
                                        </li>
                                        <li class="tick-info">
                                            Posuere cubilia Curae
                                        </li>
                                        <li class="tick-info">
                                            Nulla mollis dapibus
                                        </li>
                                        <li class="tick-info">
                                            Rhoncus turpis sodales
                                        </li>
                                    </ul>
                                </div>
                                <div class="buy-button text-center mt-5">
                                    <a href="contact.html" class="btn btn-secondary btn-theme2">
                                        <div class="anim"></div>
                                        <span>Request Now</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-lg-0 mt-4 w3l-pricing-7-gd-left">
                        <div class="w3l-pricing-7 pric-7-2">
                            <h6>Exclusive Pack</h6>
                            <div class="w3l-pricing-7-top">
                                <h3 class="title-sub">$399.00</h3>
                                <p>1 Month</p>
                            </div>
                            <div class="w3l-pricing-7-bottom">
                                <div class="w3l-pricing-7-bottom-bottom">
                                    <ul class="links">
                                        <li class="tick-info">
                                            Vestibulum ante ipsum
                                        </li>
                                        <li class="tick-info">
                                            Primis in faucibus
                                        </li>
                                        <li class="tick-info">
                                            Orci luctus et ultrices
                                        </li>
                                        <li class="tick-info">
                                            Posuere cubilia Curae
                                        </li>
                                        <li class="tick-info">
                                            Nulla mollis dapibus
                                        </li>
                                        <li class="tick-info">
                                            Rhoncus turpis sodales
                                        </li>
                                    </ul>
                                </div>
                                <div class="buy-button text-center mt-5">
                                    <a href="contact.html" class="btn btn-secondary btn-theme2">
                                        <div class="anim"></div>
                                        <span>Request Now</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section jadwal perwalian list -->

    <!-- section informasi umum -->

    <section class="w3l-features-8">
        <!-- /features -->
        <div class="features py-5" id="services">
            <div class="container py-md-3">
                <h3 class="head text-center mb-5">Informasi Umum</h3>
                <div class="fea-gd-vv text-center row">
                    <div class="float-top col-lg-4 col-md-6">
                        <a href="{{ route('landing_page_informasi_umum_detail') }}">
                            <img src="{{ asset('template/landing_page/images/g2.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="float-lt feature-gd">
                            <h3><a href="{{ route('landing_page_informasi_umum_detail') }}">Robots in Space</a> </h3>
                            <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                        </div>
                    </div>
                    <div class="float-top col-lg-4 col-md-6">
                        <a href="{{ route('landing_page_informasi_umum_detail') }}">
                            <img src="{{ asset('template/landing_page/images/g3.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="float-lt feature-gd">
                            <h3><a href="{{ route('landing_page_informasi_umum_detail') }}">Robots in Space</a> </h3>
                            <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                        </div>
                    </div>
                    <div class="float-top col-lg-4 col-md-6">
                        <a href="{{ route('landing_page_informasi_umum_detail') }}">
                            <img src="{{ asset('template/landing_page/images/g8.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="float-lt feature-gd">
                            <h3><a href="{{ route('landing_page_informasi_umum_detail') }}">Robots in Space</a> </h3>
                            <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //features -->
    </section>
    <!-- section informasi umum -->

@endsection