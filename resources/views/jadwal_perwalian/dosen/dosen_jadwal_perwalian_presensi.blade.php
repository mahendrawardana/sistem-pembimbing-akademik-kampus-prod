@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">

            <form action="{{ route('dosenJadwalPerwalianPresensiUpdate', ['id_jadwal_perwalian' => $jadwal_perwalian->id_jadwal_perwalian]) }}"
                  method="post" class="form-send"
                  data-redirect="{{ route('dosenJadwalPerwalianList') }}">

            {{ csrf_field() }}

            <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table table-striped m-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Nim
                                        </th>
                                        <th>
                                            Nama
                                        </th>
                                        <th>
                                            Keterangan
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            {{ $jadwal_perwalian->mhs_nim }}
                                        </th>
                                        <td>
                                            {{ $jadwal_perwalian->mhs_nama }}
                                        </td>
                                        <td>
                                            <div class="form-group m-form__group">
                                                <div class="m-radio-inline">
                                                    <label class="m-radio">
                                                        <input type="radio" name="jpl_presensi" value="hadir" {{ $jadwal_perwalian->jpl_presensi == 'hadir' ? 'checked':'' }}>
                                                        Hadir
                                                        <span></span>
                                                    </label>
                                                    <label class="m-radio">
                                                        <input type="radio" name="jpl_presensi" value="alpa" {{ $jadwal_perwalian->jpl_presensi == 'alpa' ? 'checked':'' }}>
                                                        Alpa
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col m--align-right">
                            <a href="{{ route('dosenJadwalPerwalianList') }}" class="btn btn-secondary">
                    <span>
                        Kembali
                    </span>
                            </a>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Form-->
                </div>
            </form>
        </div>
        <!--end::Portlet-->
    </div>

    </div>
@endsection
