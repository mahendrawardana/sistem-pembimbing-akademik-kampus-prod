@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form form-send" action="{{ route('dosenJadwalPerwalianInsert') }}" method="post"
              data-redirect="{{ route('dosenJadwalPerwalianList') }}">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Nama Mahasiswa
                            </label>
                            <select class="form-control m-input" name="id_mahasiswa" id="angkatan">
                                @foreach($mahasiswa as $row)
                                    <option value="{{ $row->id_mahasiswa }}">{{ $row->mhs_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example-time-input">
                                Tanggal
                            </label>
                            <div>
                                <input class="form-control m-input" type="date" name="jpl_date" value="{{ date('Y-m-d') }}"
                                       id="example-time-input">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example-time-input">
                                Waktu
                            </label>
                            <div>
                                <input class="form-control m-input" type="time" name="jpl_time" value="{{ date('H:i:s') }}"
                                       id="example-time-input">
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('adminJadwalPerwalianList') }}" class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
