@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('dosenJadwalPerwalianCreate') }}"
                       class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                    >
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="row">
                <div class="col-lg">
                    <!--begin::Portlet-->

                    @foreach($jadwal_perwalian as $row)

                        <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="flaticon-statistics"></i>
                            </span>
                                        <h3 class="m-portlet__head-text">
                                            {{ Main::format_date_label($row->jpl_date).' '.$row->jpl_time }}
                                        </h3>
                                        <h2 class="m-portlet__head-label m-portlet__head-label--danger">
                                <span>
                                    Jadwal Perwalian
                                </span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <p>{{ $row->mhs_nama }}</p>
                                <p>{{ $row->mhs_angkatan }}</p>
                                <p>{{ $mata_kuliah->mkl_nama }}</p>
                                <br>
                                <a href="{{ route('dosenJadwalPerwalianEdit', ['id_jadwal_perwalian' => $row->id_jadwal_perwalian]) }}" class="btn btn-primary">
                                    Edit
                                </a>
                                <a href="{{ route('dosenJadwalPerwalianPresensi', ['id_jadwal_perwalian' => $row->id_jadwal_perwalian]) }}" class="btn btn-success">
                                    Presensi
                                </a>
                                <a href="{{ route('dosenJadwalPerwalianCatatan', ['id_jadwal_perwalian' => $row->id_jadwal_perwalian]) }}" class="btn btn-warning">
                                    Catatan
                                </a>
                            </div>
                        </div>
                        <!--end::Portlet-->
                        <br>

                    @endforeach

                </div>
            </div>

        </div>

    </div>
@endsection
