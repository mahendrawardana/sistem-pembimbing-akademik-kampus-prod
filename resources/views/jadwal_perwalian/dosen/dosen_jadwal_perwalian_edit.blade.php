@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form form-send" method="post"
              action="{{ route('dosenJadwalPerwalianUpdate', ['id_jadwal_perwalian' => $edit->id_jadwal_perwalian]) }}"
                data-redirect="{{ route('dosenJadwalPerwalianList') }}">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="example-date-input">
                                Tanggal
                            </label>
                            <input class=" form-control m-input" type="date" name="jpl_date"
                                   value="{{ $edit->jpl_date }}" id="example-date-input">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example-date-input">
                                Jam
                            </label>
                            <input class=" form-control m-input" type="text" name="jpl_time"
                                   value="{{ $edit->jpl_time }}" id="example-date-input">
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('dosenJadwalPerwalianList') }}" class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Perbarui
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
