@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <form class="form-send"
                  action="{{ route('dosenJadwalPerwalianCatatanUpdate', ['id_jadwal_perwalian' => $jadwal_perwalian->id_jadwal_perwalian]) }}"
                  method="post"
                  data-redirect="{{ route('dosenJadwalPerwalianList') }}">

                {{ csrf_field() }}

                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">

                        <h5>
                            {{ $jadwal_perwalian->mhs_nim.' / '.$jadwal_perwalian->mhs_nama }}
                        </h5>
                        <div class="form-group m-form__group">
                            <label for="alamat">
                                Catatan Perwalian
                            </label>
                            <textarea class="form-control m-input" name="jpl_catatan" id="alamat" rows="6">{{ $jadwal_perwalian->jpl_catatan }}</textarea>
                        </div>

                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('dosenJadwalPerwalianList') }}" class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>
@endsection
