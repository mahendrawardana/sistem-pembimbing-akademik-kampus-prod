@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <div class="row">
                <div class="col-lg">

                @foreach($jadwal_perwalian as $row)

                    <!--begin::Portlet-->
                        <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="flaticon-statistics"></i>
                            </span>
                                        <h3 class="m-portlet__head-text">
                                            {{ Main::format_date_label($row->jpl_date).' '.$row->jpl_time }}
                                        </h3>
                                        <h2 class="m-portlet__head-label m-portlet__head-label--danger">
                                <span>
                                    Jadwal Perwalian
                                </span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <p>Dosen {{ $row->dsn_nama }}</p>
                                <p>{{ $row->mhs_angkatan }}</p>
                                <br>
                                <a href="{{ route('mahasiswaJadwalPerwalianCatatan', ['id_jadwal_perwalian' => $row->id_jadwal_perwalian]) }}"
                                   class="btn btn-warning">
                                    Catatan
                                </a>
                            </div>
                        </div>
                        <br/>

                @endforeach

                <!--end::Portlet-->
                </div>
            </div>
        </div>

    </div>
@endsection
