@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form form-send" method="post"
              action="{{ route('mata_kuliah_update', ['id_mata_kuliah'=> $row->id_mata_kuliah]) }}"
              data-redirect="{{ route('mata_kuliah_list') }}">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="nim">
                                Kode Matakuliah
                            </label>
                            <input type="text" class="form-control m-input" name="mkl_kode"
                                   placeholder="Masukan kode matakuliah anda" value="{{ $row->mkl_kode }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="nama">
                                Nama Matakuliah
                            </label>
                            <input type="text" class="form-control m-input" name="mkl_nama"
                                   placeholder="Masukan nama matakuliah anda" value="{{ $row->mkl_nama }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="nama">
                                SKS
                            </label>
                            <input type="text" class="form-control m-input" name="mkl_sks"
                                   placeholder="Masukan sks anda" value="{{ $row->mkl_sks }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Nama Dosen
                            </label>
                            <select class="form-control m-input" name="id_dosen" id="angkatan">
                                @foreach($dosen as $row_dosen)
                                    <option value="{{ $row_dosen->id_dosen }}" {{ $row->id_dosen == $row_dosen->id_dosen ? 'selected':'' }}>{{ $row_dosen->dsn_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="example-date-input">
                                Hari
                            </label>
                            <input class=" form-control m-input" name="mkl_hari" type="date" value="{{ date('Y-m-d', strtotime($row->mkl_hari)) }}"
                                   id="example-date-input">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Group
                            </label>
                            <input type="text" class="form-control" name="mkl_group" value="{{ $row->mkl_group }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Sesi
                            </label>
                            <input type="number" class="form-control" name="mkl_sesi" value="{{ $row->mkl_sesi }}" min="1">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Ruangan
                            </label>
                            <input type="text" class="form-control" name="mkl_ruangan" value="{{ $row->mkl_ruangan }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="dosen">
                                Harga
                            </label>
                            <input type="number" class="form-control m-input" name="mkl_harga"
                                   placeholder="Masukan harga" value="{{ $row->mkl_harga }}">
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('mata_kuliah_list') }}" class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Perbarui
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
