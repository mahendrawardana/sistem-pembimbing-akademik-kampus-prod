@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('informasi_umum_create') }}"
                       class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       >
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped m-table">
                        <thead>
                        <tr>
                            <th>
                                No
                            </th>
                            <th>
                                Judul Informasi
                            </th>
                            <th>
                                Isi Informasi
                            </th>
                            <th>
                                Aksi
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">
                                1
                            </th>
                            <td>
                                Judul
                            </td>
                            <td>
                                Isi
                            </td>
                            <td>
                                <div class="btn-group" role="group">
                                    <button id="btnGroupVerticalDrop1" type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                        <a class="dropdown-item" href="{{ route('informasi_umum_edit') }}">
                                            Edit
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Hapus
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                2
                            </th>
                            <td>
                                Judul
                            </td>
                            <td>
                                Isi
                            </td>
                            <td>
                                <div class="btn-group" role="group">
                                    <button id="btnGroupVerticalDrop1" type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
                                        <a class="dropdown-item" href="{{ route('informasi_umum_edit') }}">
                                            Edit
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            Hapus
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
