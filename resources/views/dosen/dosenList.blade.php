@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('dosen_create') }}"
                       class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                    >
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-striped m-table datatable-new-2">
                        <thead>
                        <tr>
                            <th>
                                NIK
                            </th>
                            <th>
                                Nama Dosen
                            </th>
                            <th>
                                Jabatan
                            </th>
                            <th>
                                Jenis Kelamin
                            </th>
                            <th>
                                No. Telp
                            </th>
                            <th>
                                Alamat
                            </th>
                            <th>
                                Aksi
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dosen as $row)
                            <tr>
                                <th scope="row">
                                    {{ $row->dsn_nik }}
                                </th>
                                <td>
                                    {{ $row->dsn_nama }}
                                </td>
                                <td>
                                    {{ $row->dsn_jabatan }}
                                </td>
                                <td>
                                    {{ $row->dsn_jenis_kelamin }}
                                </td>
                                <td>
                                    {{ $row->dsn_phone }}
                                </td>
                                <td>
                                    {{ $row->dsn_alamat }}
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                                                type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Menu
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"
                                             aria-labelledby="dropdownMenuButton">
                                            <a class="akses-edit dropdown-item"
                                               href="{{ route('dosen_edit', ['id_dosen'=> $row->id_dosen]) }}">
                                                <i class="la la-pencil"></i>
                                                Edit
                                            </a>
                                            <a class="akses-delete dropdown-item btn-hapus"
                                               href="#"
                                               data-route="{{ route('dosen_delete', ['id_dosen' => $row->id_dosen]) }}">
                                                <i class="la la-remove"></i>
                                                Hapus
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
