@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form form-send" method="post" action="{{ route('dosen_update', ['id_dosen' => $row->id_dosen]) }}" data-redirect="{{ route('dosen_list') }}">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="nim">
                                NIK
                            </label>
                            <input type="text" class="form-control m-input" name="dsn_nik" value="{{ $row->dsn_nik }}" placeholder="Masukan NIK anda">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="nama">
                                Nama Dosen
                            </label>
                            <input type="text" class="form-control m-input" name="dsn_nama" value="{{ $row->dsn_nama }}" placeholder="Masukan nama anda">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="jabatan">
                                Jabatan
                            </label>
                            <input type="text" class="form-control m-input" name="dsn_jabatan" value="{{ $row->dsn_jabatan }}" id="jabatan">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="jeniskelamin">
                                Jenis Kelamin
                            </label>
                            <select class="form-control m-input" name="dsn_jenis_kelamin" id="jeniskelamin">
                                <option value="pria" {{ $row->dsn_jenis_kelamin == 'pria' ? 'selected':'' }}>
                                    Pria
                                </option>
                                <option value="wanita" {{ $row->dsn_jenis_kelamin == 'wanita' ? 'selected':'' }}>
                                    Perempuan
                                </option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="telp">
                                No Telp
                            </label>
                            <input type="text" name="dsn_phone" value="{{ $row->dsn_phone }}" class="form-control m-input" placeholder="Masukan no telp anda">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="alamat">
                                Alamat
                            </label>
                            <textarea class="form-control m-input" name="dsn_alamat" id="alamat" rows="3">{{ $row->dsn_alamat }}</textarea>
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('dosen_list') }}" class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Perbarui
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
