@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <?php if(!$mahasiswa->krs_status) { ?>
                    <a href="{{ route('mahasiswa_krs_create') }}"
                       class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                    >
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Data</span>
                        </span>
                    </a>
                    <a href="{{ route('mahasiswa_krs_ajukan') }}"
                       class="akses-create btn btn-success m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                    >
                        <span>
                            <i class="la la-check"></i>
                            <span>Ajukan</span>
                        </span>
                    </a>
                    <?php } else {

                    } ?>
                </div>
            </div>
        </div>


        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Kartu Rencana Studi -
                                @if($mahasiswa->krs_status == 'ajukan')
                                    Sudah Diajukan
                                @elseif($mahasiswa->krs_status == 'diterima')
                                    Diterima
                                @elseif($mahasiswa->krs_status == 'ditolak')
                                    Ditolak
                                @else
                                    Belum Mengajukan
                                @endif
                            </h3>
                        </div>
                        <div></div>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            <h3 class="text-center">
                                Kartu Rencana Studi
                            </h3>
                            <h4 class="text-center">{{ $mahasiswa->mhs_nama }}</h4>
                            <br>
                            <table class="table table-striped m-table datatable-new-2">
                                <thead>
                                <tr>
                                    <th>
                                        Kode
                                    </th>
                                    <th>
                                        Nama Matakuliah
                                    </th>
                                    <th>
                                        SKS
                                    </th>
                                    <th>
                                        Harga
                                    </th>
                                    <th>
                                        Group
                                    </th>
                                    <th>
                                        Sesi
                                    </th>
                                    <th>
                                        Hari
                                    </th>
                                    <th>
                                        Ruangan
                                    </th>
                                    <th>
                                        Dosen
                                    </th>
                                    <th>
                                        Menu
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($krs as $row)
                                    <tr>
                                        <th scope="row">
                                            {{ $row->mata_kuliah->mkl_kode }}
                                        </th>
                                        <td>
                                            {{ $row->mata_kuliah->mkl_nama }}
                                        </td>
                                        <td>
                                            {{ $row->mata_kuliah->mkl_sks }}
                                        </td>
                                        <td>
                                            {{ $row->mata_kuliah->mkl_harga }}
                                        </td>
                                        <td>
                                            {{ $row->mata_kuliah->mkl_group }}
                                        </td>
                                        <td>
                                            {{ $row->mata_kuliah->mkl_sesi }}
                                        </td>
                                        <td>
                                            {{ $row->mata_kuliah->mkl_hari }}
                                        </td>
                                        <td>
                                            {{ $row->mata_kuliah->mkl_ruangan }}
                                        </td>
                                        <td>
                                            {{ $row->mata_kuliah->dosen->dsn_nama }}
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                                                        type="button"
                                                        id="dropdownMenuButton" data-toggle="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="false">
                                                    Menu
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right"
                                                     aria-labelledby="dropdownMenuButton">
                                                    <a class="akses-edit dropdown-item"
                                                       href="{{ route('mahasiswa_krs_edit', ['id_krs'=> $row->id_krs]) }}">
                                                        <i class="la la-pencil"></i>
                                                        Edit
                                                    </a>
                                                    <a class="akses-delete dropdown-item btn-hapus"
                                                       href="#"
                                                       data-route="{{ route('mahasiswa_krs_delete', ['id_krs' => $row->id_krs]) }}">
                                                        <i class="la la-remove"></i>
                                                        Hapus
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
@endsection
