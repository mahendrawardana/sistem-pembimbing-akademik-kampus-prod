@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form form-send"
              method="post"
              action="{{ route('mahasiswa_krs_insert') }}"
              data-redirect="{{ route('mahasiswa_krs_list') }}">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="text">
                                Nama Mata Kuliah
                            </label>
                            <select class="form-control" name="id_mata_kuliah">
                                @foreach($mata_kuliah as $row)
                                    <option value="{{ $row->id_mata_kuliah }}">{{ $row->mkl_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Angakatan
                            </label>
                            <input type="text" name="krs_angkatan" class="form-control">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Periode
                            </label>
                            <input type="text" name="krs_periode" class="form-control">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="angkatan">
                                Semester
                            </label>
                            <select class="form-control m-input" name="krs_semester" id="angkatan">
                                <option value="Genap">Genap</option>
                                <option value="Ganjil">Ganjil</option>
                            </select>
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('admin_krs_list') }}" class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
