@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>


        <form class="m-form">
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <h3 class="text-center">
                            Kartu Rencana Studi
                        </h3>
                        <h4 class="text-center">{{ $mahasiswa->mhs_nama }}</h4>
                        <br>
                        <table class="table table-striped m-table">
                            <thead>
                            <tr>
                                <th>
                                    Kode
                                </th>
                                <th>
                                    Nama Matakuliah
                                </th>
                                <th>
                                    SKS
                                </th>
                                <th>
                                    Harga
                                </th>
                                <th>
                                    Group
                                </th>
                                <th>
                                    Sesi
                                </th>
                                <th>
                                    Hari
                                </th>
                                <th>
                                    Ruangan
                                </th>
                                <th>
                                    Dosen
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($krs as $row)
                            <tr>
                                <th scope="row">
                                    {{ $row->mata_kuliah->mkl_kode }}
                                </th>
                                <td>
                                    {{ $row->mata_kuliah->mkl_nama }}
                                </td>
                                <td>
                                    {{ $row->mata_kuliah->mkl_sks }}
                                </td>
                                <td>
                                    {{ $row->mata_kuliah->mkl_harga }}
                                </td>
                                <td>
                                    {{ $row->mata_kuliah->mkl_group }}
                                </td>
                                <td>
                                    {{ $row->mata_kuliah->mkl_sesi }}
                                </td>
                                <td>
                                    {{ $row->mata_kuliah->mkl_hari }}
                                </td>
                                <td>
                                    {{ $row->mata_kuliah->mkl_ruangan }}
                                </td>
                                <td>
                                    {{ $row->mata_kuliah->dosen->dsn_nama }}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
