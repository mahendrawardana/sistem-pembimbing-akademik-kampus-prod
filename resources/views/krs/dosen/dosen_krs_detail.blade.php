@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                @if($mahasiswa->krs_status == 'ajukan')
                    <div>
                        <a href="{{ route('dosen_krs_setuju', ['id_mahasiswa' => $mahasiswa->id_mahasiswa]) }}"
                           class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                        >
                        <span>
                            <i class="la la-check"></i>
                            <span>ACC KRS</span>
                        </span>
                        </a>
                        <a href="{{ route('dosen_krs_tolak', ['id_mahasiswa' => $mahasiswa->id_mahasiswa]) }}"
                           class="akses-create btn btn-danger m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                        >
                        <span>
                            <i class="la la-remove"></i>
                            <span>Tolak KRS</span>
                        </span>
                        </a>
                    </div>
                @endif
            </div>
        </div>


        <form class="m-form">
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Kartu Rencana Studi -
                                    @if($mahasiswa->krs_status == 'ajukan')
                                        Sudah Diajukan
                                    @elseif($mahasiswa->krs_status == 'diterima')
                                        Diterima
                                    @elseif($mahasiswa->krs_status == 'ditolak')
                                        Ditolak
                                    @else
                                        Belum Mengajukan
                                    @endif
                                </h3>
                            </div>
                            <div></div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <h3 class="text-center">
                            Kartu Rencana Studi
                        </h3>
                        <h4 class="text-center">{{ $mahasiswa->mhs_nama }}</h4>
                        <br>
                        <table class="table table-striped m-table">
                            <thead>
                            <tr>
                                <th>
                                    Kode
                                </th>
                                <th>
                                    Nama Matakuliah
                                </th>
                                <th>
                                    SKS
                                </th>
                                <th>
                                    Harga
                                </th>
                                <th>
                                    Group
                                </th>
                                <th>
                                    Sesi
                                </th>
                                <th>
                                    Hari
                                </th>
                                <th>
                                    Ruangan
                                </th>
                                <th>
                                    Dosen
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($krs as $row)
                                <tr>
                                    <th scope="row">
                                        {{ $row->mata_kuliah->mkl_kode }}
                                    </th>
                                    <td>
                                        {{ $row->mata_kuliah->mkl_nama }}
                                    </td>
                                    <td>
                                        {{ $row->mata_kuliah->mkl_sks }}
                                    </td>
                                    <td>
                                        {{ $row->mata_kuliah->mkl_harga }}
                                    </td>
                                    <td>
                                        {{ $row->mata_kuliah->mkl_group }}
                                    </td>
                                    <td>
                                        {{ $row->mata_kuliah->mkl_sesi }}
                                    </td>
                                    <td>
                                        {{ $row->mata_kuliah->mkl_hari }}
                                    </td>
                                    <td>
                                        {{ $row->mata_kuliah->mkl_ruangan }}
                                    </td>
                                    <td>
                                        {{ $row->mata_kuliah->dosen->dsn_nama }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
