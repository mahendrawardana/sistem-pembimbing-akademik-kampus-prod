@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form form-send" action="{{ route('admin_mahasiswa_update', ['id_mahasiswa' => $row->id_mahasiswa]) }}" method="post" data-redirect="{{ route('admin_mahasiswa_list') }}">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="input_nim">
                                NIM
                            </label>
                            <input type="text" class="form-control m-input" name="mhs_nim" value="{{ $row->mhs_nim }}" placeholder="Masukan NIM">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="input_nama">
                                Nama
                            </label>
                            <input type="text" class="form-control m-input" name="mhs_nama" value="{{ $row->mhs_nama }}" placeholder="Masukan nama">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="select_angkatan">
                                Angakatan/Tahun
                            </label>
                            <input type="text" class="form-control m-input" name="mhs_angkatan" value="{{ $row->mhs_angkata }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="checkbox_status">
                                Status
                            </label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="mhs_status" value="aktif" {{ $row->mhs_status == 'aktif' ? 'checked':'' }}>
                                    Aktif
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="mhs_status" value="tidak_aktif" {{ $row->mhs_status == 'tidak_aktif' ? 'checked':'' }}>
                                    Tidak Aktif
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="input_telp">
                                No Telp
                            </label>
                            <input type="text" class="form-control m-input" name="mhs_phone" value="{{ $row->mhs_phone }}" placeholder="Masukan no telp">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="textarea_alamat">
                                Alamat
                            </label>
                            <textarea class="form-control m-input" id="alamat" name="mhs_alamat" rows="3">{{ $row->mhs_alamat }}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="dosen">
                                Dosen Wali
                            </label>
                            <select class="form-control" name="id_dosen">
                                @foreach($dosen as $row)
                                    <option value="{{ $row->id_dosen }}" {{ $row->id_dosen == $row->id_dosen ? 'selected':'' }}>{{ $row->dsn_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('admin_mahasiswa_list') }}" class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
