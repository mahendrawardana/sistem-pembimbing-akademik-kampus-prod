@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">

                    <table class="table table-striped m-table">
                        <thead>
                        <tr>
                            <th>
                                Nim
                            </th>
                            <th>
                                Nama
                            </th>
                            <th>
                                Angkatan
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                No. Telp
                            </th>
                            <th>
                                Alamat
                            </th>
                            <th>
                                Dosen Wali
                            </th>
                            <th>
                                Aksi
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">
                                72170109
                            </th>
                            <td>
                                Zefanya Anke
                            </td>
                            <td>
                                2017
                            </td>
                            <td>
                                Aktif
                            </td>
                            <td>
                                085346251
                            </td>
                            <td>
                                Kalimantan
                            </td>
                            <td>
                                Jong Jek Siang
                            </td>
                            <td>
                                <a href="{{ route('dosen_mahasiswa_nilai') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">Lihat Nilai</a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                72170109
                            </th>
                            <td>
                                Zefanya Anke
                            </td>
                            <td>
                                2017
                            </td>
                            <td>
                                Aktif
                            </td>
                            <td>
                                085346251
                            </td>
                            <td>
                                Kalimantan
                            </td>
                            <td>
                                Jong Jek Siang
                            </td>
                            <td>
                                <a href="{{ route('dosen_mahasiswa_nilai') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">Lihat Nilai</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
