@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form">
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <table class="table table-striped m-table">
                            <thead>
                            <tr>
                                <th>
                                    Kode
                                </th>
                                <th>
                                    Nama Mata Kuliah
                                </th>
                                <th>
                                    SKS
                                </th>
                                <th>
                                    Harga
                                </th>
                                <th>
                                    Group
                                </th>
                                <th>
                                    SKS
                                </th>
                                <th>
                                    Nilai
                                </th>
                                <th>
                                    Bobot
                                </th>
                                <th>
                                    Kualitas
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">
                                    SK323
                                </th>
                                <td>
                                    Etika Profesi
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    A
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    B
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    9
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    SI242
                                </th>
                                <td>
                                    Sistem Informasi Akuntasi
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    B
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    B+
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    9
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
