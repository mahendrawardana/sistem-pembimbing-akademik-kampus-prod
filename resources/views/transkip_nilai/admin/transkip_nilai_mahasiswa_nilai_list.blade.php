@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="{{ route('transkip_nilai_mahasiswa_list') }}"
                       class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                    >
                        <span>
                            <i class="la la-share-alt"></i>
                            <span>Kembali</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <form class="m-form">
            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <table class="table table-striped m-table datatable-new-2">
                            <thead>
                            <tr>
                                <th>
                                    Kode
                                </th>
                                <th>
                                    Nama Mata Kuliah
                                </th>
                                <th>
                                    SKS
                                </th>
                                <th>
                                    Harga
                                </th>
                                <th>
                                    Nilai
                                </th>
                                <th>
                                    Bobot
                                </th>
                                <th>
                                    Kualitas
                                </th>
                                <th>
                                    Menu
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($krs as $row)
                            <tr>
                                <th scope="row">
                                    {{ $row->mkl_kode }}
                                </th>
                                <td>
                                    {{ $row->mkl_nama }}
                                </td>
                                <td>
                                    {{ $row->mkl_sks }}
                                </td>
                                <td>
                                    {{ $row->mkl_harga }}
                                </td>
                                <td>
                                    {{ $row->tsn_nilai }}
                                </td>
                                <td>
                                    {{ $row->tsn_bobot }}
                                </td>
                                <td>
                                    {{ $row->tsn_kualitas }}
                                </td>
                                <td>
                                    <a href="{{ route('transkip_nilai_mahasiswa_nilai_edit', ['id_krs' => $row->id_krs]) }}" class="btn btn-success">Edit</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </form>

    </div>
@endsection
