@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">

                    <table class="table table-striped m-table datatable-new-2">
                        <thead>
                        <tr>
                            <th>
                                Nim
                            </th>
                            <th>
                                Nama Mahasiswa
                            </th>
                            <th>
                                Angkatan
                            </th>
                            <th>
                                Status
                            </th>
                            <th width="100">
                                Aksi
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mahasiswa as $row)
                        <tr>
                            <th scope="row">
                                {{ $row->mhs_nim }}
                            </th>
                            <td>
                                {{ $row->mhs_nama }}
                            </td>
                            <td>
                                {{ $row->mhs_angkatan }}
                            </td>
                            <td>
                                {{ $row->mhs_status }}
                            </td>
                            <td>
                                <div>
                                    <a href="{{ route('transkip_nilai_mahasiswa_nilai_list', ['id_mahasiswa' => $row->id_mahasiswa]) }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                                            <span>
                                                Lihat Nilai
                                            </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
