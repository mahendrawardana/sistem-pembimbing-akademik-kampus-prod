@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <form class="m-form form-send" method="post"
              action="{{ route('transkip_nilai_mahasiswa_nilai_update', ['id_krs' => $id_krs]) }}"
              data-redirect="{{ route('transkip_nilai_mahasiswa_nilai_list', ['id_mahasiswa' => $krs->id_mahasiswa]) }}"
        >

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="dosen">
                                Nilai
                            </label>
                            <input type="text" class="form-control m-input" name="tsn_nilai" value="{{ $transkip_nilai->tsn_nilai }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="nama">
                                Bobot
                            </label>
                            <input type="text" class="form-control m-input" name="tsn_bobot" value="{{ $transkip_nilai->tsn_bobot }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="nama">
                                Kualitas
                            </label>
                            <input type="text" class="form-control m-input" name="tsn_kualitas" value="{{ $transkip_nilai->tsn_kualitas }}">
                        </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--right">
                            <div class="row">
                                <div class="col m--align-right">
                                    <a href="{{ route('transkip_nilai_mahasiswa_nilai_list', ['id_mahasiswa' => $krs->id_mahasiswa]) }}"
                                       class="btn btn-secondary">
                                <span>
                                    Kembali
                                </span>
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        Perbarui
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
