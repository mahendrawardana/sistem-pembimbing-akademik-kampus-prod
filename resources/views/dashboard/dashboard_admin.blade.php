@extends('../general/index')

@section('js')
    <script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>

@endsection

@section('css')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css"/>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">

            <div class="row">
                <a href="{{ route('karyawanList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Mahasiswa
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light" >
                                        <span>{{ $total_profit }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('karyawanList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Dosen
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span>{{ $total_pembelian }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('karyawanList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Mata Kuliah
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span>{{ $total_penjualan }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('karyawanList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Jadwal Perwalian
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span>{{ $total_stok_alert }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>


        </div>
    </div>
@endsection
