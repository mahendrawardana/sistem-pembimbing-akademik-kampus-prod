<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mMataKuliah extends Model
{
    use SoftDeletes;
    protected $table = 'mata_kuliah';
    protected $primaryKey = 'id_mata_kuliah';
    protected $fillable = [
        'id_dosen',
        'mkl_kode',
        'mkl_nama',
        'mkl_sks',
        'mkl_hari',
        'mkl_group',
        'mkl_sesi',
        'mkl_ruangan',
        'mkl_harga',
    ];

    public function dosen()
    {
        return $this->belongsTo(mDosen::class, 'id_dosen');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
