<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mTranskipNilai extends Model
{
    use SoftDeletes;
    protected $table = 'transkip_nilai';
    protected $primaryKey = 'id_transkip_nilai';
    protected $fillable = [
        'id_mahasiswa',
        'id_mata_kuliah',
        'id_krs',
        'tsn_nilai',
        'tsn_bobot',
        'tsn_kualitas',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
