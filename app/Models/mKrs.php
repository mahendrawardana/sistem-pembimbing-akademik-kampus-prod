<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mKrs extends Model
{
    use SoftDeletes;
    protected $table = 'krs';
    protected $primaryKey = 'id_krs';
    protected $fillable = [
        'id_mahasiswa',
        'id_mata_kuliah',
        'krs_angkatan',
        'krs_periode',
        'krs_semester',
        'krs_status',
    ];

    public function mahasiswa() {
        return $this->belongsTo(mMahasiswa::class, 'id_mahasiswa');
    }

    public function mata_kuliah() {
        return $this->belongsTo(mMataKuliah::class, 'id_mata_kuliah');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
