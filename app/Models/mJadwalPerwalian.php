<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mJadwalPerwalian extends Model
{
    use SoftDeletes;
    protected $table = 'jadwal_perwalian';
    protected $primaryKey = 'id_jadwal_perwalian';
    protected $fillable = [
        'id_dosen',
        'id_mahasiswa',
        'jpl_date',
        'jpl_time',
        'jpl_presensi',
        'jpl_catatan',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
