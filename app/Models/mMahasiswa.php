<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mMahasiswa extends Model
{
    use SoftDeletes;
    protected $table = 'mahasiswa';
    protected $primaryKey = 'id_mahasiswa';
    protected $fillable = [
        'id_dosen',
        'krs_status',
        'mhs_nim',
        'mhs_nama',
        'mhs_angkatan',
        'mhs_status',
        'mhs_phone',
        'mhs_alamat',
    ];

    public function dosen()
    {
        return $this->belongsTo(mDosen::class, 'id_dosen');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
