<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHistoryPenyesuaianStok;
use app\Models\mHutangSupplier;
use app\Models\mHutangSupplierPembayaran;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HutangSupplier extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['hutang_supplier'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['hutang_supplier'],
                'route' => route('hutangSupplierList')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur_hutang"],
//            ["data" => "faktur_pembelian"],
            ["data" => "tanggal_hutang"],
            ["data" => "jatuh_tempo"],
            ["data" => "total_hutang"],
            ["data" => "sisa"],
            ["data" => "keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),
        ]);

        return view('hutangPiutang/hutangSupplier/hutangSupplierList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mHutangSupplier
            ::where([
                'hsp_status' => 'belum_lunas'
            ])
            ->whereBetween('hsp_tanggal', $where_date)
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_hutang_supplier'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mHutangSupplier
            ::with([
                'pembelian'
            ])
            ->where('hsp_status', 'belum_lunas')
            ->whereBetween('hsp_tanggal', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_hutang_supplier = Main::encrypt($row->id_hutang_supplier);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur_hutang'] = $row->hsp_no_faktur;
//            $nestedData['faktur_pembelian'] = $row->pembelian->pbl_no_faktur;
            $nestedData['tanggal_hutang'] = Main::format_date_label($row->hsp_tanggal);
            $nestedData['jatuh_tempo'] = Main::format_date_label($row->hsp_tanggal_jatuh_tempo);
            $nestedData['total_hutang'] = Main::format_number($row->hsp_total);
            $nestedData['sisa'] = Main::format_number($row->hsp_sisa);
            $nestedData['keterangan'] = $row->hsp_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('hutangSupplierPembayaranModal', ['id_hutang_supplier' => $id_hutang_supplier]) . '">
                            <i class="la la-list"></i>
                            Lakukan Pembayaran
                        </a>
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('hutangSupplierPembayaranHistoryModal', ['id_hutang_supplier' => $id_hutang_supplier]) . '">
                            <i class="la la-list"></i>
                            History Pembayaran
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function index_lunas(Request $request)
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);

        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur_hutang"],
//            ["data" => "faktur_pembelian"],
            ["data" => "tanggal_hutang"],
            ["data" => "jatuh_tempo"],
            ["data" => "total_hutang"],
            ["data" => "sisa"],
            ["data" => "keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),
        ]);

        return view('hutangPiutang/hutangSupplier/hutangSupplierLunasList', $data);
    }

    function data_table_lunas(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mHutangSupplier
            ::where([
                'hsp_status' => 'lunas'
            ])
            ->whereBetween('hsp_tanggal', $where_date)
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_hutang_supplier'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mHutangSupplier
            ::with([
                'pembelian'
            ])
            ->where('hsp_status', 'lunas')
            ->whereBetween('hsp_tanggal', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_hutang_supplier = Main::encrypt($row->id_hutang_supplier);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur_hutang'] = $row->hsp_no_faktur;
//            $nestedData['faktur_pembelian'] = $row->pembelian->pbl_no_faktur;
            $nestedData['tanggal_hutang'] = Main::format_date_label($row->hsp_tanggal);
            $nestedData['jatuh_tempo'] = Main::format_date_label($row->hsp_tanggal_jatuh_tempo);
            $nestedData['total_hutang'] = Main::format_number($row->hsp_total);
            $nestedData['sisa'] = Main::format_number($row->hsp_sisa);
            $nestedData['keterangan'] = $row->hsp_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('hutangSupplierPembayaranHistoryModal', ['id_hutang_supplier' => $id_hutang_supplier]) . '">
                            <i class="la la-list"></i>
                            History Pembayaran
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function pembayaran_modal($id_hutang_supplier)
    {
        $id_hutang_supplier = Main::decrypt($id_hutang_supplier);
        $row = mHutangSupplier
            ::with([
                'supplier',
                'pembelian'
            ])
            ->where('id_hutang_supplier', $id_hutang_supplier)
            ->first();

        $data = [
            'row' => $row,
        ];

        return view('hutangPiutang.hutangSupplier.hutangSupplierPembayaranModal', $data);
    }

    function pembayaran_insert(Request $request)
    {
        $request->validate([
            'hsp_tanggal_bayar' => 'required',
            'hsp_keterangan' => 'required',
            'hsp_jumlah_bayar' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $user = Session::get('user');
            $id_user = $user['id'];

            $id_hutang_supplier = $request->input('id_hutang_supplier');
            $hsp_total_hutang = $request->input('hsp_total_hutang');
            $hsp_sisa_pembayaran = $request->input('hsp_sisa_pembayaran');
            $hsp_tanggal_bayar = $request->input('hsp_tanggal_bayar');
            $hsp_keterangan = $request->input('hsp_keterangan');
            $hsp_jumlah_bayar = Main::format_number_db($request->input('hsp_jumlah_bayar'));

            $hutang_supplier_pembayaran = [
                'id_user' => $id_user,
                'id_hutang_supplier' => $id_hutang_supplier,
                'hsp_total_hutang' => $hsp_total_hutang,
                'hsp_jumlah_bayar' => $hsp_jumlah_bayar,
                'hsp_sisa_bayar' => $hsp_sisa_pembayaran,
                'hsp_tanggal_bayar' => Main::format_date_db($hsp_tanggal_bayar),
                'hsp_keterangan' => $hsp_keterangan
            ];

            mHutangSupplierPembayaran::create($hutang_supplier_pembayaran);

            $hutang_supplier_data = [
                'hsp_sisa' => $hsp_sisa_pembayaran
            ];

            if ($hsp_sisa_pembayaran == 0) {
                $hutang_supplier_data['hsp_status'] = 'lunas';
            }

            mHutangSupplier::where('id_hutang_supplier', $id_hutang_supplier)->update($hutang_supplier_data);


            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }


    }

    function pembayaran_history_modal($id_hutang_supplier)
    {
        $id_hutang_supplier = Main::decrypt($id_hutang_supplier);
        $data = mHutangSupplierPembayaran
            ::with([
                'user',
            ])
            ->where('id_hutang_supplier', $id_hutang_supplier)
            ->get();
        $hutang_supplier = mHutangSupplier
            ::with([
                'pembelian',
                'supplier'
            ])
            ->where('id_hutang_supplier', $id_hutang_supplier)
            ->first();

        $data = [
            'data' => $data,
            'hutang_supplier' => $hutang_supplier
        ];

        return view('hutangPiutang.hutangSupplier.hutangSupplierPembayaranHistoryModal', $data);
    }

}
