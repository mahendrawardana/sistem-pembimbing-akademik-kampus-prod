<?php

namespace app\Http\Controllers\Barang;

use app\Models\mBarang;
use app\Models\mJenisBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class Barang extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['master_barang'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mBarang
            ::with([
                'jenis_barang'
            ])
            ->orderBy('brg_kode', 'ASC')
            ->get();
        $jenis_barang = mJenisBarang::orderBy('jbr_nama', 'ASC')->get();
        $datatable_column = [
            ["data" => "no"],
            ["data" => "brg_nama"],
            ["data" => "jbr_nama"],
            ["data" => "brg_golongan"],
            ["data" => "brg_minimal_stok"],
            ["data" => "total_stok"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'jenis_barang' => $jenis_barang,
            'datatable_column' => $datatable_column
        ]);

        return view('barang/barang/barangList', $data);
    }


    function data_table(Request $request)
    {

        $total_data = mBarang
            ::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'brg_kode'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mBarang
            ::with('jenis_barang')
            ->withCount([
                'stok_barang AS total_stok' => function($query) {
                    $query->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang = Main::encrypt($row->id_barang);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['brg_nama'] = $row->brg_kode . ' ' . $row->brg_nama;
            $nestedData['jbr_nama'] = $row->jenis_barang->jbr_nama;
            $nestedData['brg_golongan'] = Main::barang_golongan_label($row->brg_golongan);
            $nestedData['brg_minimal_stok'] = Main::format_number($row->brg_minimal_stok);
            $nestedData['total_stok'] = Main::format_number($row->total_stok);
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done dropdown-item"
                           href="' . route('stokBarangList', ['id_barang' => $id_barang]) . '">
                            <i class="la la-list"></i>
                            Daftar Stok Barang
                        </a>
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('barangEditModal', ['id_barang' => $id_barang]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus"
                           href="#"
                            data-route="' . route('barangDelete', ['id_barang' => $id_barang]) . '">
                            <i class="la la-remove"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_jenis_barang' => 'required',
            'brg_kode' => 'required',
            'brg_nama' => 'required',
            'brg_golongan' => 'required',
            'brg_minimal_stok' => 'required',
            'brg_keterangan' => 'required',
        ]);

        $data = $request->except('_token');
        mBarang::create($data);
    }

    function edit_modal($id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        $edit = mBarang::where('id_barang', $id_barang)->first();
        $jenis_barang = mJenisBarang::orderBy('jbr_nama', 'ASC')->get();
        $data = [
            'edit' => $edit,
            'jenis_barang' => $jenis_barang
        ];

        return view('barang/barang/barangEditModal', $data);
    }

    function delete($id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        mBarang::where('id_barang', $id_barang)->delete();
    }

    function update(Request $request, $id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        $request->validate([
            'id_jenis_barang' => 'required',
            'brg_kode' => 'required',
            'brg_nama' => 'required',
            'brg_golongan' => 'required',
            'brg_minimal_stok' => 'required',
            'brg_keterangan' => 'required',
        ]);
        $data = $request->except("_token");
        mBarang::where(['id_barang' => $id_barang])->update($data);
    }
}
