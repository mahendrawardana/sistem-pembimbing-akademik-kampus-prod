<?php

namespace app\Http\Controllers\General;

use app\Models\mBarang;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPenjualan;
use app\Models\mPiutangPelanggan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $total_barang = 0;
        $total_pelanggan = 0;
        $total_pembelian = 0;
        $total_penjualan = 0;
        $total_stok_alert = 0;
        $total_hutang_supplier = 0;
        $total_piutang_pelanggan = 0;
        $total_profit = $total_penjualan - $total_pembelian;

        $data = array_merge($data, array(
            'total_barang' => Main::format_number($total_barang),
            'total_pelanggan' => Main::format_number($total_pelanggan),
            'total_pembelian' => Main::format_number($total_pembelian),
            'total_penjualan' => Main::format_number($total_penjualan),
            'total_stok_alert' => Main::format_number($total_stok_alert),
            'total_hutang_supplier' => Main::format_number($total_hutang_supplier),
            'total_piutang_pelanggan' => Main::format_number($total_piutang_pelanggan),
            'total_profit' => Main::format_number($total_profit),
        ));

        return view('dashboard/dashboard_admin', $data);

    }

    function whatsapp_test()
    {
        Main::whatsappSend('+6281934364063', 'HELLO,, ini adalah test message ' . date('d-m-Y H:i:s'));
    }


}
