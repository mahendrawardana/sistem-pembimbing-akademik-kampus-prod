<?php

namespace app\Http\Controllers\Dosen;

use app\Models\mBarang;
use app\Models\mDosen;
use app\Models\mJenisBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class Dosen extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['dosen'],
                'route' => ''
            ]
        ];
    }


    function index()
    {
        $data = Main::data($this->breadcrumb);
        $dosen = mDosen::orderBy('dsn_nama', 'ASC')->get();

        $data = array_merge($data, [
            'dosen' => $dosen
        ]);

        return view('dosen/dosenList', $data);
    }

    function create()
    {
        $data = Main::data($this->breadcrumb);
        return view('dosen/dosenCreate', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'dsn_nik' => 'required',
            'dsn_nama' => 'required',
            'dsn_jabatan' => 'required',
            'dsn_jenis_kelamin' => 'required',
            'dsn_phone' => 'required',
            'dsn_alamat' => 'required',
        ]);

        $dsn_nik = $request->input('dsn_nik');
        $dsn_nama = $request->input('dsn_nama');
        $dsn_jabatan = $request->input('dsn_jabatan');
        $dsn_jenis_kelamin = $request->input('dsn_jenis_kelamin');
        $dsn_phone = $request->input('dsn_phone');
        $dsn_alamat = $request->input('dsn_alamat');

        $data_insert = [
            'dsn_nik' => $dsn_nik,
            'dsn_nama' => $dsn_nama,
            'dsn_jabatan' => $dsn_jabatan,
            'dsn_jenis_kelamin' => $dsn_jenis_kelamin,
            'dsn_phone' => $dsn_phone,
            'dsn_alamat' => $dsn_alamat
        ];

        mDosen::create($data_insert);
    }

    function edit($id_dosen)
    {
        $data = Main::data($this->breadcrumb);
        $row = mDosen::where('id_dosen', $id_dosen)->first();

        $data = array_merge($data, [
            'row' => $row
        ]);

        return view('dosen/dosenEdit', $data);
    }

    function update(Request $request, $id_dosen)
    {
        $request->validate([
            'dsn_nik' => 'required',
            'dsn_nama' => 'required',
            'dsn_jabatan' => 'required',
            'dsn_jenis_kelamin' => 'required',
            'dsn_phone' => 'required',
            'dsn_alamat' => 'required',
        ]);

        $dsn_nik = $request->input('dsn_nik');
        $dsn_nama = $request->input('dsn_nama');
        $dsn_jabatan = $request->input('dsn_jabatan');
        $dsn_jenis_kelamin = $request->input('dsn_jenis_kelamin');
        $dsn_phone = $request->input('dsn_phone');
        $dsn_alamat = $request->input('dsn_alamat');

        $data_update = [
            'dsn_nik' => $dsn_nik,
            'dsn_nama' => $dsn_nama,
            'dsn_jabatan' => $dsn_jabatan,
            'dsn_jenis_kelamin' => $dsn_jenis_kelamin,
            'dsn_phone' => $dsn_phone,
            'dsn_alamat' => $dsn_alamat
        ];

        mDosen::where('id_dosen', $id_dosen)->update($data_update);
    }

    function delete($id_dosen)
    {
        mDosen::where('id_dosen', $id_dosen)->delete();
    }
}
