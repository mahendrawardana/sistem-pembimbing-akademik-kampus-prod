<?php

namespace app\Http\Controllers\LandingPage;

use app\Models\mUserRole;
use app\Rules\LoginRolesCheck;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Rules\LoginCheck;
use Illuminate\Support\Facades\Log;
use app\Models\mUser;
use Illuminate\Support\Facades\Session;

class LandingPage extends Controller
{
    function home()
    {
        $data = [];

        return view('landing_page/beranda', $data);
    }

    function informasi_umum_list()
    {
        $data = [];

        return view('landing_page/informasi_umum_list', $data);
    }

    function informasi_umum_detail()
    {
        $data = [];

        return view('landing_page/informasi_umum_detail', $data);
    }

    function kontak_kami()
    {
        $data = [];

        return view('landing_page/kontak_kami', $data);
    }
}
