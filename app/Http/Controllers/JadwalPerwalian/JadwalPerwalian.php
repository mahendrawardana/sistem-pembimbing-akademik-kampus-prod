<?php

namespace app\Http\Controllers\JadwalPerwalian;

use app\Models\mBarang;
use app\Models\mDosen;
use app\Models\mJadwalPerwalian;
use app\Models\mJenisBarang;
use app\Models\mMahasiswa;
use app\Models\mMataKuliah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class JadwalPerwalian extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['jadwal_perwalian_admin'],
                'route' => ''
            ]
        ];
    }


    function admin_list()
    {
        $data = Main::data($this->breadcrumb);

        $jadwal_perwalian = mJadwalPerwalian
            ::leftJoin('dosen', 'dosen.id_dosen', '=', 'jadwal_perwalian.id_dosen')
            ->leftJoin('mahasiswa', 'mahasiswa.id_mahasiswa', '=', 'jadwal_perwalian.id_mahasiswa')
            ->get();

        $data = array_merge($data, [
            'jadwal_perwalian' => $jadwal_perwalian
        ]);

        return view('jadwal_perwalian/admin/admin_jadwal_perwalian_list', $data);
    }

    function admin_create()
    {
        $data = Main::data($this->breadcrumb);
        $dosen = mDosen::get();
        $mahasiswa = mMahasiswa::get();

        $data = array_merge($data, [
            'dosen' => $dosen,
            'mahasiswa' => $mahasiswa
        ]);


        return view('jadwal_perwalian/admin/admin_jadwal_perwalian_create', $data);
    }

    function admin_insert(Request $request)
    {
        $request->validate([
            'id_dosen' => 'required',
            'id_mahasiswa' => 'required',
            'jpl_date' => 'required',
            'jpl_time' => 'required'
        ]);

        $id_dosen = $request->input('id_dosen');
        $id_mahasiswa = $request->input('id_mahasiswa');
        $jpl_date = $request->input('jpl_date');
        $jpl_time = $request->input('jpl_time');

        $data_insert = [
            'id_dosen' => $id_dosen,
            'id_mahasiswa' => $id_mahasiswa,
            'jpl_date' => $jpl_date,
            'jpl_time' => $jpl_time
        ];

        mJadwalPerwalian::create($data_insert);
    }

    function admin_edit($id_jadwal_perwalian)
    {
        $data = Main::data($this->breadcrumb);
        $dosen = mDosen::get();
        $mahasiswa = mMahasiswa::get();
        $edit = mJadwalPerwalian::where('id_jadwal_perwalian', $id_jadwal_perwalian)->first();

        $data = array_merge($data, [
            'dosen' => $dosen,
            'mahasiswa' => $mahasiswa,
            'edit' => $edit
        ]);


        return view('jadwal_perwalian/admin/admin_jadwal_perwalian_edit', $data);
    }

    function admin_update(Request $request, $id_jadwal_perwalian)
    {
        $request->validate([
            'id_dosen' => 'required',
            'id_mahasiswa' => 'required',
            'jpl_date' => 'required',
            'jpl_time' => 'required'
        ]);

        $id_dosen = $request->input('id_dosen');
        $id_mahasiswa = $request->input('id_mahasiswa');
        $jpl_date = $request->input('jpl_date');
        $jpl_time = $request->input('jpl_time');

        $data_update = [
            'id_dosen' => $id_dosen,
            'id_mahasiswa' => $id_mahasiswa,
            'jpl_date' => $jpl_date,
            'jpl_time' => $jpl_time
        ];

        mJadwalPerwalian::where('id_jadwal_perwalian', $id_jadwal_perwalian)->update($data_update);
    }

    function admin_delete($id_jadwal_perwalian)
    {
        mJadwalPerwalian::where('id_jadwal_perwalian', $id_jadwal_perwalian)->delete();
    }


    function dosen_list()
    {
        $data = Main::data($this->breadcrumb);

        $id_dosen = $data['user']->id_dosen;
        $jadwal_perwalian = mJadwalPerwalian
            ::leftJoin('mahasiswa', 'mahasiswa.id_mahasiswa', '=', 'jadwal_perwalian.id_mahasiswa')
            ->where('jadwal_perwalian.id_dosen', $id_dosen)
            ->orderBy('id_jadwal_perwalian', 'DESC')
            ->get();
        $mata_kuliah = mMataKuliah::where('id_dosen', $id_dosen)->first();

        $data = array_merge($data, [
            'jadwal_perwalian' => $jadwal_perwalian,
            'mata_kuliah' => $mata_kuliah
        ]);

        return view('jadwal_perwalian/dosen/dosen_jadwal_perwalian_list', $data);
    }

    function dosen_create()
    {
        $data = Main::data($this->breadcrumb);
        $dosen = mDosen::get();
        $mahasiswa = mMahasiswa::get();

        $data = array_merge($data, [
            'dosen' => $dosen,
            'mahasiswa' => $mahasiswa
        ]);


        return view('jadwal_perwalian/dosen/dosen_jadwal_perwalian_create', $data);
    }

    function dosen_insert(Request $request)
    {
        $data = Main::data($this->breadcrumb);
        $id_dosen = $data['user']->id_dosen;

        $data_insert = [
            'id_dosen' => $id_dosen,
            'id_mahasiswa' => $request->input('id_mahasiswa'),
            'jpl_date' => $request->input('jpl_date'),
            'jpl_time' => $request->input('jpl_time')
        ];

        mJadwalPerwalian::create($data_insert);
    }

    function dosen_edit($id_jadwal_perwalian)
    {
        $data = Main::data($this->breadcrumb);
        $edit = mJadwalPerwalian::where('id_jadwal_perwalian', $id_jadwal_perwalian)->first();

        $data = array_merge($data, [
            'edit' => $edit
        ]);

        return view('jadwal_perwalian/dosen/dosen_jadwal_perwalian_edit', $data);
    }

    function dosen_update(Request $request, $id_jadwal_perwalian)
    {
        $jpl_date = $request->input('jpl_date');
        $jpl_time = $request->input('jpl_time');

        $data_update = [
            'jpl_date' => $jpl_date,
            'jpl_time' => $jpl_time
        ];

        mJadwalPerwalian::where('id_jadwal_perwalian', $id_jadwal_perwalian)->update($data_update);
    }

    function dosen_presensi($id_jadwal_perwalian)
    {
        $data = Main::data($this->breadcrumb);
        $jadwal_perwalian = mJadwalPerwalian
            ::leftJoin('mahasiswa', 'mahasiswa.id_mahasiswa', '=', 'jadwal_perwalian.id_mahasiswa')
            ->where('id_jadwal_perwalian', $id_jadwal_perwalian)
            ->first();

        $data = array_merge($data, [
            'jadwal_perwalian' => $jadwal_perwalian
        ]);


        return view('jadwal_perwalian/dosen/dosen_jadwal_perwalian_presensi', $data);
    }

    function dosen_presensi_update(Request $request, $id_jadwal_perwalian)
    {
        $jpl_presensi = $request->input('jpl_presensi');

        mJadwalPerwalian
            ::where('id_jadwal_perwalian', $id_jadwal_perwalian)
            ->update([
                'jpl_presensi' => $jpl_presensi
            ]);
    }

    function dosen_catatan($id_jadwal_perwalian)
    {
        $data = Main::data($this->breadcrumb);
        $jadwal_perwalian = mJadwalPerwalian
            ::leftJoin('mahasiswa', 'mahasiswa.id_mahasiswa', '=', 'jadwal_perwalian.id_mahasiswa')
            ->where('id_jadwal_perwalian', $id_jadwal_perwalian)
            ->first();

        $data = array_merge($data, [
            'jadwal_perwalian' => $jadwal_perwalian
        ]);

        return view('jadwal_perwalian/dosen/dosen_jadwal_perwalian_catatan', $data);
    }

    function dosen_catatan_update(Request $request, $id_jadwal_perwalian)
    {
        $jpl_catatan = $request->input('jpl_catatan');

        mJadwalPerwalian::where('id_jadwal_perwalian', $id_jadwal_perwalian)->update(['jpl_catatan' => $jpl_catatan]);
    }

    function mahasiswa_list()
    {
        $data = Main::data($this->breadcrumb);
        $id_mahasiswa = $data['user']->id_mahasiswa;

        $jadwal_perwalian = mJadwalPerwalian
            ::leftJoin('mahasiswa', 'mahasiswa.id_mahasiswa', '=', 'jadwal_perwalian.id_mahasiswa')
            ->leftJoin('dosen', 'dosen.id_dosen', '=', 'jadwal_perwalian.id_dosen')
            ->where('jadwal_perwalian.id_mahasiswa', $id_mahasiswa)
            ->orderBy('id_jadwal_perwalian', 'DESC')
            ->get();

        $data = array_merge($data, [
            'jadwal_perwalian' => $jadwal_perwalian
        ]);

        return view('jadwal_perwalian/mahasiswa/mahasiswa_jadwal_perwalian_list', $data);
    }

    function mahasiswa_catatan($id_jadwal_perwalian)
    {
        $data = Main::data($this->breadcrumb);
        $jadwal_perwalian = mJadwalPerwalian
            ::leftJoin('mahasiswa', 'mahasiswa.id_mahasiswa', '=', 'jadwal_perwalian.id_mahasiswa')
            ->leftJoin('dosen', 'dosen.id_dosen', '=', 'jadwal_perwalian.id_dosen')
            ->where('jadwal_perwalian.id_jadwal_perwalian', $id_jadwal_perwalian)
            ->first();

        $data = array_merge($data, [
            'jadwal_perwalian' => $jadwal_perwalian
        ]);

        return view('jadwal_perwalian/mahasiswa/mahasiswa_jadwal_perwalian_catatan', $data);
    }

}
