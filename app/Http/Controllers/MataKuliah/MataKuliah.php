<?php

namespace app\Http\Controllers\MataKuliah;

use app\Models\mBarang;
use app\Models\mDosen;
use app\Models\mJenisBarang;
use app\Models\mMataKuliah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class MataKuliah extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['mata_kuliah'],
                'route' => ''
            ]
        ];
    }


    function index()
    {
        $data = Main::data($this->breadcrumb);
        $mata_kuliah = mMataKuliah::orderBy('mkl_nama')->get();

        $data = array_merge($data, [
            'mata_kuliah' => $mata_kuliah
        ]);

        return view('mataKuliah/mataKuliahList', $data);
    }

    function create()
    {
        $data = Main::data($this->breadcrumb);
        $dosen = mDosen::orderBy('dsn_nama')->get();

        $data = array_merge($data, [
            'dosen' => $dosen
        ]);

        return view('mataKuliah/mataKuliahCreate', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'mkl_kode' => 'required',
            'mkl_nama' => 'required',
            'mkl_sks' => 'required',
            'id_dosen' => 'required',
            'mkl_hari' => 'required',
            'mkl_group' => 'required',
            'mkl_sesi' => 'required',
            'mkl_ruangan' => 'required',
            'mkl_harga' => 'required'
        ]);

        $mkl_kode = $request->input('mkl_kode');
        $mkl_nama = $request->input('mkl_nama');
        $mkl_sks = $request->input('mkl_sks');
        $id_dosen = $request->input('id_dosen');
        $mkl_hari = $request->input('mkl_hari');
        $mkl_group = $request->input('mkl_group');
        $mkl_sesi = $request->input('mkl_sesi');
        $mkl_ruangan = $request->input('mkl_ruangan');
        $mkl_harga = $request->input('mkl_harga');

        $data_insert = [
            'mkl_kode' => $mkl_kode,
            'mkl_nama' => $mkl_nama,
            'mkl_sks' => $mkl_sks,
            'id_dosen' => $id_dosen,
            'mkl_hari' => $mkl_hari,
            'mkl_group' => $mkl_group,
            'mkl_sesi' => $mkl_sesi,
            'mkl_ruangan' => $mkl_ruangan,
            'mkl_harga' => $mkl_harga
        ];

        mMataKuliah::insert($data_insert);
    }

    function edit($id_mata_kuliah)
    {
        $data = Main::data($this->breadcrumb);
        $row = mMataKuliah::where('id_mata_kuliah', $id_mata_kuliah)->first();
        $dosen = mDosen::orderBy('dsn_nama')->get();

        $data = array_merge($data, [
            'row' => $row,
            'dosen' => $dosen
        ]);

        return view('mataKuliah/mataKuliahEdit', $data);
    }

    function update(Request $request, $id_mata_kuliah)
    {
        $request->validate([
            'mkl_kode' => 'required',
            'mkl_nama' => 'required',
            'mkl_sks' => 'required',
            'id_dosen' => 'required',
            'mkl_hari' => 'required',
            'mkl_group' => 'required',
            'mkl_sesi' => 'required',
            'mkl_ruangan' => 'required',
            'mkl_harga' => 'required'
        ]);

        $mkl_kode = $request->input('mkl_kode');
        $mkl_nama = $request->input('mkl_nama');
        $mkl_sks = $request->input('mkl_sks');
        $id_dosen = $request->input('id_dosen');
        $mkl_hari = $request->input('mkl_hari');
        $mkl_group = $request->input('mkl_group');
        $mkl_sesi = $request->input('mkl_sesi');
        $mkl_ruangan = $request->input('mkl_ruangan');
        $mkl_harga = $request->input('mkl_harga');

        $data_update = [
            'mkl_kode' => $mkl_kode,
            'mkl_nama' => $mkl_nama,
            'mkl_sks' => $mkl_sks,
            'id_dosen' => $id_dosen,
            'mkl_hari' => $mkl_hari,
            'mkl_group' => $mkl_group,
            'mkl_sesi' => $mkl_sesi,
            'mkl_ruangan' => $mkl_ruangan,
            'mkl_harga' => $mkl_harga
        ];

        mMataKuliah::where('id_mata_kuliah', $id_mata_kuliah)->update($data_update);
    }

    function delete($id_mata_kuliah)
    {
        mMataKuliah::where('id_mata_kuliah', $id_mata_kuliah)->delete();
    }
}
