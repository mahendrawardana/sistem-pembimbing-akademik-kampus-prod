<?php

namespace app\Http\Controllers\Krs;

use app\Models\mBarang;
use app\Models\mJenisBarang;
use app\Models\mKrs;
use app\Models\mMahasiswa;
use app\Models\mMataKuliah;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class Krs extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['krs'],
                'route' => ''
            ]
        ];
    }

    function admin_list()
    {
        $data = Main::data($this->breadcrumb);
        $krs = mKrs::get();

        $data = array_merge($data, [
            'krs' => $krs
        ]);

        return view('krs/admin/admin_krs_list', $data);
    }

    function admin_create()
    {
        $data = Main::data($this->breadcrumb);
        $mahasiswa = mMahasiswa::orderBy('mhs_nama', 'ASC')->get();
        $mata_kuliah = mMataKuliah::orderBy('mkl_nama', 'ASC')->get();

        $data = array_merge($data, [
            'mahasiswa' => $mahasiswa,
            'mata_kuliah' => $mata_kuliah
        ]);


        return view('krs/admin/admin_krs_create', $data);
    }

    function admin_insert(Request $request)
    {
        $request->validate([
            'id_mahasiswa' => 'required',
            'id_mata_kuliah' => 'required',
            'krs_angkatan' => 'required',
            'krs_periode' => 'required',
            'krs_semester' => 'required',
        ]);

        $id_mahasiswa = $request->input('id_mahasiswa');
        $id_mata_kuliah = $request->input('id_mata_kuliah');
        $krs_angkatan = $request->input('krs_angkatan');
        $krs_periode = $request->input('krs_periode');
        $krs_semester = $request->input('krs_semester');

        $data_insert = [
            'id_mahasiswa' => $id_mahasiswa,
            'id_mata_kuliah' => $id_mata_kuliah,
            'krs_angkatan' => $krs_angkatan,
            'krs_periode' => $krs_periode,
            'krs_semester' => $krs_semester
        ];

        mKrs::create($data_insert);
    }

    function admin_edit($id_krs)
    {
        $data = Main::data($this->breadcrumb);

        $mahasiswa = mMahasiswa::orderBy('mhs_nama', 'ASC')->get();
        $mata_kuliah = mMataKuliah::orderBy('mkl_nama', 'ASC')->get();
        $edit = mKrs::where('id_krs', $id_krs)->first();

        $data = array_merge($data, [
            'mahasiswa' => $mahasiswa,
            'mata_kuliah' => $mata_kuliah,
            'edit' => $edit
        ]);

        return view('krs/admin/admin_krs_edit', $data);
    }

    function admin_update(Request $request, $id_krs)
    {
        $request->validate([
            'id_mahasiswa' => 'required',
            'id_mata_kuliah' => 'required',
            'krs_angkatan' => 'required',
            'krs_periode' => 'required',
            'krs_semester' => 'required',
        ]);

        $id_mahasiswa = $request->input('id_mahasiswa');
        $id_mata_kuliah = $request->input('id_mata_kuliah');
        $krs_angkatan = $request->input('krs_angkatan');
        $krs_periode = $request->input('krs_periode');
        $krs_semester = $request->input('krs_semester');

        $data_update = [
            'id_mahasiswa' => $id_mahasiswa,
            'id_mata_kuliah' => $id_mata_kuliah,
            'krs_angkatan' => $krs_angkatan,
            'krs_periode' => $krs_periode,
            'krs_semester' => $krs_semester
        ];

        mKrs::where('id_krs', $id_krs)->update($data_update);
    }

    function admin_delete($id_krs)
    {
        mKrs::where('id_krs', $id_krs)->delete();
    }

    function admin_detail($id_mahasiswa)
    {
        $data = Main::data($this->breadcrumb);
        $krs = mKrs::where('id_mahasiswa', $id_mahasiswa)->get();
        $mahasiswa = mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->first();

        $data = array_merge($data, [
            'krs' => $krs,
            'mahasiswa' => $mahasiswa
        ]);

        return view('krs/admin/admin_krs_detail', $data);
    }

    function dosen_list()
    {
        $data = Main::data($this->breadcrumb);
        $mahasiswa = mMahasiswa::get();

        $data = array_merge($data, [
            'mahasiswa' => $mahasiswa
        ]);

        return view('krs/dosen/dosen_krs_list', $data);
    }

    function dosen_detail($id_mahasiswa)
    {
        $data = Main::data($this->breadcrumb);
        $krs = mKrs::where('id_mahasiswa', $id_mahasiswa)->get();
        $mahasiswa = mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->first();

        $data = array_merge($data, [
            'krs' => $krs,
            'mahasiswa' => $mahasiswa
        ]);


        return view('krs/dosen/dosen_krs_detail', $data);
    }

    function dosen_setuju($id_mahasiswa)
    {
        mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->update(['krs_status' => 'diterima']);
        return redirect()->route('dosen_krs_list');
    }

    function dosen_tolak($id_mahasiswa)
    {
        mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->update(['krs_status' => 'ditolak']);
        return redirect()->route('dosen_krs_list');
    }

    function mahasiswa_list()
    {
        $data = Main::data($this->breadcrumb);
        $id_mahasiswa = $data['user']->id_mahasiswa;
        $mahasiswa = mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->first();
        $krs = mKrs::where('id_mahasiswa', $id_mahasiswa)->get();

        $data = array_merge($data, [
            'krs' => $krs,
            'mahasiswa' => $mahasiswa
        ]);


        return view('krs/mahasiswa/mahasiswa_krs_list', $data);
    }

    function mahasiswa_create()
    {
        $data = Main::data($this->breadcrumb);
        $mata_kuliah = mMataKuliah::get();

        $data = array_merge($data, [
            'mata_kuliah' => $mata_kuliah
        ]);


        return view('krs/mahasiswa/mahasiswa_krs_create', $data);
    }

    function mahasiswa_insert(Request $request)
    {
        $request->validate([
            'id_mata_kuliah' => 'required',
            'krs_angkatan' => 'required',
            'krs_periode' => 'required',
            'krs_semester' => 'required',
        ]);

        $data = Main::data($this->breadcrumb);
        $id_mahasiswa = $data['user']->id_mahasiswa;
        $id_mata_kuliah = $request->input('id_mata_kuliah');
        $krs_angkatan = $request->input('krs_angkatan');
        $krs_periode = $request->input('krs_periode');
        $krs_semester = $request->input('krs_semester');

        $data_insert = [
            'id_mahasiswa' => $id_mahasiswa,
            'id_mata_kuliah' => $id_mata_kuliah,
            'krs_angkatan' => $krs_angkatan,
            'krs_periode' => $krs_periode,
            'krs_semester' => $krs_semester
        ];

        mKrs::create($data_insert);
    }

    function mahasiswa_edit($id_krs)
    {
        $data = Main::data($this->breadcrumb);
        $mata_kuliah = mMataKuliah::get();
        $edit = mKrs::where('id_krs', $id_krs)->first();

        $data = array_merge($data, [
            'mata_kuliah' => $mata_kuliah,
            'edit' => $edit
        ]);


        return view('krs/mahasiswa/mahasiswa_krs_edit', $data);
    }

    function mahasiswa_update(Request $request, $id_krs)
    {
        $request->validate([
            'id_mata_kuliah' => 'required',
            'krs_angkatan' => 'required',
            'krs_periode' => 'required',
            'krs_semester' => 'required',
        ]);

        $id_mata_kuliah = $request->input('id_mata_kuliah');
        $krs_angkatan = $request->input('krs_angkatan');
        $krs_periode = $request->input('krs_periode');
        $krs_semester = $request->input('krs_semester');

        $data_update = [
            'id_mata_kuliah' => $id_mata_kuliah,
            'krs_angkatan' => $krs_angkatan,
            'krs_periode' => $krs_periode,
            'krs_semester' => $krs_semester
        ];

        mKrs::where('id_krs', $id_krs)->update($data_update);
    }

    function mahasiswa_delete($id_krs)
    {
        mKrs::where('id_krs', $id_krs)->delete();
    }

    function mahasiswa_ajukan()
    {
        $data = Main::data($this->breadcrumb);
        $id_mahasiswa = $data['user']->id_mahasiswa;

        mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->update(['krs_status' => 'ajukan']);

        return redirect()->route('mahasiswa_krs_list');
    }

}
