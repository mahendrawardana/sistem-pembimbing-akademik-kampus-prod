<?php

namespace app\Http\Controllers\Mahasiswa;

use app\Models\mBarang;
use app\Models\mDosen;
use app\Models\mJenisBarang;
use app\Models\mMahasiswa;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class Mahasiswa extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['mahasiswa'],
                'route' => ''
            ]
        ];
    }

    function admin_mahasiswa_list()
    {
        $data = Main::data($this->breadcrumb);

        $mahasiswa = mMahasiswa::orderBy('mhs_nama', 'ASC')->get();

        $data = array_merge($data, [
            'mahasiswa' => $mahasiswa
        ]);

        return view('mahasiswa/admin/admin_mahasiswa_list', $data);
    }

    function admin_mahasiswa_create()
    {
        $data = Main::data($this->breadcrumb);
        $dosen = mDosen::orderBy('dsn_nama', 'ASC')->get();

        $data = array_merge($data, [
            'dosen' => $dosen
        ]);

        return view('mahasiswa/admin/admin_mahasiswa_create', $data);
    }

    function admin_mahasiswa_insert(Request $request)
    {
        $request->validate([
            'id_dosen' => 'required',
            'mhs_nim' => 'required',
            'mhs_nama' => 'required',
            'mhs_angkatan' => 'required',
            'mhs_status' => 'required',
            'mhs_phone' => 'required',
            'mhs_alamat' => 'required',
        ]);

        $id_dosen = $request->input('id_dosen');
        $mhs_nim = $request->input('mhs_nim');
        $mhs_nama = $request->input('mhs_nama');
        $mhs_angkatan = $request->input('mhs_angkatan');
        $mhs_status = $request->input('mhs_status');
        $mhs_phone = $request->input('mhs_phone');
        $mhs_alamat = $request->input('mhs_alamat');

        $data_insert = [
            'id_dosen' => $id_dosen,
            'mhs_nim' => $mhs_nim,
            'mhs_nama' => $mhs_nama,
            'mhs_angkatan' => $mhs_angkatan,
            'mhs_status' => $mhs_status,
            'mhs_phone' => $mhs_phone,
            'mhs_alamat' => $mhs_alamat
        ];

        mMahasiswa::create($data_insert);
    }

    function admin_mahasiswa_edit($id_mahasiswa)
    {
        $data = Main::data($this->breadcrumb);
        $row = mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->first();
        $dosen = mDosen::orderBy('dsn_nama', 'ASC')->get();

        $data = array_merge($data, [
            'row' => $row,
            'dosen' => $dosen
        ]);

        return view('mahasiswa/admin/admin_mahasiswa_edit', $data);
    }

    function admin_mahasiswa_update(Request $request, $id_mahasiswa)
    {
        $request->validate([
            'id_dosen' => 'required',
            'mhs_nim' => 'required',
            'mhs_nama' => 'required',
            'mhs_angkatan' => 'required',
            'mhs_status' => 'required',
            'mhs_phone' => 'required',
            'mhs_alamat' => 'required',
        ]);

        $id_dosen = $request->input('id_dosen');
        $mhs_nim = $request->input('mhs_nim');
        $mhs_nama = $request->input('mhs_nama');
        $mhs_angkatan = $request->input('mhs_angkatan');
        $mhs_status = $request->input('mhs_status');
        $mhs_phone = $request->input('mhs_phone');
        $mhs_alamat = $request->input('mhs_alamat');

        $data_update = [
            'id_dosen' => $id_dosen,
            'mhs_nim' => $mhs_nim,
            'mhs_nama' => $mhs_nama,
            'mhs_angkatan' => $mhs_angkatan,
            'mhs_status' => $mhs_status,
            'mhs_phone' => $mhs_phone,
            'mhs_alamat' => $mhs_alamat
        ];

        mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->update($data_update);
    }

    function admin_mahasiswa_delete($id_mahasiswa)
    {
        mMahasiswa::where('id_mahasiswa', $id_mahasiswa)->delete();
    }

    function dosen_mahasiswa_list()
    {
        $data = Main::data($this->breadcrumb);
        return view('mahasiswa/dosen/dosen_mahasiswa_list', $data);
    }

    function dosen_mahasiswa_nilai()
    {
        $data = Main::data($this->breadcrumb);
        return view('mahasiswa/dosen/dosen_mahasiswa_nilai', $data);
    }

}
