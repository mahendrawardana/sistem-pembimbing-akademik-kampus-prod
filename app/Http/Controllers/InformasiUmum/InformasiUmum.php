<?php

namespace app\Http\Controllers\InformasiUmum;

use app\Models\mBarang;
use app\Models\mJenisBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class InformasiUmum extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['informasi_umum'],
                'route' => ''
            ]
        ];
    }



    function list()
    {
        $data = Main::data($this->breadcrumb);
        return view('informasi_umum/informasi_umum_list', $data);
    }

    function create()
    {
        $data = Main::data($this->breadcrumb);
        return view('informasi_umum/informasi_umum_create', $data);
    }

    function edit()
    {
        $data = Main::data($this->breadcrumb);
        return view('informasi_umum/informasi_umum_edit', $data);
    }
}
