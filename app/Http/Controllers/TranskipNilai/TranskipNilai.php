<?php

namespace app\Http\Controllers\TranskipNilai;

use app\Models\mBarang;
use app\Models\mJenisBarang;
use app\Models\mKrs;
use app\Models\mMahasiswa;
use app\Models\mMataKuliah;
use app\Models\mTranskipNilai;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class TranskipNilai extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['transkip_nilai'],
                'route' => ''
            ]
        ];
    }

    function mahasiswa_list()
    {
        $data = Main::data($this->breadcrumb);
        $mahasiswa = mMahasiswa::get();

        $data = array_merge($data, [
            'mahasiswa' => $mahasiswa
        ]);

        return view('transkip_nilai/admin/transkip_nilai_mahasiswa_list', $data);
    }

    function nilai_list($id_mahasiswa)
    {
        $data = Main::data($this->breadcrumb);
        $krs = mKrs
            ::select([
                'krs.*',
                'mata_kuliah.mkl_kode',
                'mata_kuliah.mkl_nama',
                'mata_kuliah.mkl_sks',
                'mata_kuliah.mkl_harga',
                'transkip_nilai.tsn_nilai',
                'transkip_nilai.tsn_bobot',
                'transkip_nilai.tsn_kualitas',
            ])
            ->leftJoin('mata_kuliah', 'mata_kuliah.id_mata_kuliah', '=', 'krs.id_mata_kuliah')
            ->leftJoin('transkip_nilai', 'transkip_nilai.id_krs', '=', 'krs.id_krs')
            ->where('krs.id_mahasiswa', $id_mahasiswa)
            ->get();

        $data = array_merge($data, [
            'krs' => $krs,
            'id_mahasiswa' => $id_mahasiswa
        ]);


        return view('transkip_nilai/admin/transkip_nilai_mahasiswa_nilai_list', $data);
    }

    function nilai_edit($id_krs)
    {
        $data = Main::data($this->breadcrumb);

        $check = mTranskipNilai::where('id_krs', $id_krs)->count();
        $krs = mKrs::where('id_krs', $id_krs)->first();
        if ($check == 0) {
            $transkip_nilai = (object)array(
                'tsn_nilai' => '',
                'tsn_bobot' => '',
                'tsn_kualitas' => '',
            );
        } else {
            $transkip_nilai = mTranskipNilai::where('id_krs', $id_krs)->first();
        }

        $data = array_merge($data, [
            'transkip_nilai' => $transkip_nilai,
            'id_krs' => $id_krs,
            'krs' => $krs
        ]);


        return view('transkip_nilai/admin/transkip_nilai_mahasiswa_nilai_edit', $data);
    }

    function nilai_update(Request $request, $id_krs)
    {
        $request->validate([
            'tsn_nilai' => 'required',
            'tsn_bobot' => 'required',
            'tsn_kualitas' => 'required'
        ]);

        $tsn_nilai = $request->input('tsn_nilai');
        $tsn_bobot = $request->input('tsn_bobot');
        $tsn_kualitas = $request->input('tsn_kualitas');
        $krs = mKrs::where('id_krs', $id_krs)->first();

        $check = mTranskipNilai::where('id_krs', $id_krs)->count();

        if ($check > 0) {
            $data_update = [
                'tsn_nilai' => $tsn_nilai,
                'tsn_bobot' => $tsn_bobot,
                'tsn_kualitas' => $tsn_kualitas
            ];

            mTranskipNilai
                ::where([
                    'id_mahasiswa' => $krs->id_mahasiswa,
                    'id_mata_kuliah' => $krs->id_mata_kuliah
                ])
                ->update($data_update);
        } else {
            $data_insert = [
                'id_mahasiswa' => $krs->id_mahasiswa,
                'id_mata_kuliah' => $krs->id_mata_kuliah,
                'id_krs' => $id_krs,
                'tsn_nilai' => $tsn_nilai,
                'tsn_bobot' => $tsn_bobot,
                'tsn_kualitas' => $tsn_kualitas
            ];

            mTranskipNilai::create($data_insert);
        }
    }

    function nilai_delete($id_krs)
    {
        $krs = mKrs
            ::where([
                'id_krs' => $id_krs
            ])
            ->first();

        $id_mahasiswa = $krs->id_mahasiswa;
        $id_mata_kuliah = $krs->id_mata_kuliah;

        mTranskipNilai::where([
            'id_mahasiswa' => $id_mahasiswa,
            'id_mata_kuliah' => $id_mata_kuliah
        ])->delete();
    }


    function mahasiswa_nilai_detail()
    {
        $data = Main::data($this->breadcrumb);
        $id_mahasiswa = $data['user']->id_mahasiswa;
        $krs = mKrs
            ::leftJoin('mata_kuliah', 'mata_kuliah.id_mata_kuliah', '=', 'krs.id_mata_kuliah')
            ->leftJoin('transkip_nilai', 'transkip_nilai.id_krs', '=', 'krs.id_krs')
            ->where('krs.id_mahasiswa', $id_mahasiswa)
            ->get();

        $data = array_merge($data, [
            'krs' => $krs
        ]);

        return view('transkip_nilai/mahasiswa/transkip_nilai_mahasiswa_detail', $data);
    }




    ////////////// OLD ///////////////////////
    ///
    ///
    ///


    function nilai_create($id_mahasiswa)
    {
        $data = Main::data($this->breadcrumb);
        $mata_kuliah = mMataKuliah::get();

        $data = array_merge($data, [
            'id_mahasiswa' => $id_mahasiswa,
            'mata_kuliah' => $mata_kuliah
        ]);

        return view('transkip_nilai/admin/transkip_nilai_mahasiswa_nilai_create', $data);
    }

    function nilai_insert(Request $request, $id_mahasiswa)
    {
        $request->validate([
            'id_mata_kuliah' => 'required',
            'tsn_nilai' => 'required',
            'tsn_bobot' => 'required',
            'tsn_kualitas' => 'required'
        ]);

        $id_mata_kuliah = $request->input('id_mata_kuliah');
        $tsn_nilai = $request->input('tsn_nilai');
        $tsn_bobot = $request->input('tsn_bobot');
        $tsn_kualitas = $request->input('tsn_kualitas');

        $id_krs = mKrs
            ::where([
                'id_mata_kuliah' => $id_mata_kuliah,
                'id_mahasiswa' => $id_mahasiswa
            ])
            ->value('id_krs');

        $data_insert = [
            'id_mahasiswa' => $id_mahasiswa,
            'id_mata_kuliah' => $id_mata_kuliah,
            'id_krs' => $id_krs,
            'tsn_nilai' => $tsn_nilai,
            'tsn_bobot' => $tsn_bobot,
            'tsn_kualitas' => $tsn_kualitas
        ];

        mTranskipNilai::create($data_insert);
    }

}
